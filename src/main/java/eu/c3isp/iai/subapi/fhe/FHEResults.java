package eu.c3isp.iai.subapi.fhe;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FHEResults {

	@JsonProperty("dpoIp")
	private String dpoIp;
	@JsonProperty("dpoResult")
	private String dpoResult;

	public FHEResults() {
	}

	public String getDpoIp() {
		return dpoIp;
	}

	public void setDpoIp(String dpoIp) {
		this.dpoIp = dpoIp;
	}

	public String getDpoResult() {
		return dpoResult;
	}

	public void setDpoResult(String dpoResult) {
		this.dpoResult = dpoResult;
	}

}

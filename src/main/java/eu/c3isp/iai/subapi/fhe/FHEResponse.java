package eu.c3isp.iai.subapi.fhe;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FHEResponse {

	@JsonProperty("fheResults")
	private List<FHEResults> fheResults;
	@JsonProperty("sessionId")
	private String sessionId;

	public FHEResponse() {
	}

	public List<FHEResults> getFheResults() {
		return fheResults;
	}

	public void setFheResults(List<FHEResults> fheResults) {
		this.fheResults = fheResults;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "FHEResponse [fheResults=" + fheResults + ", sessionId=" + sessionId + "]";
	}

}

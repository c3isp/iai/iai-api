package eu.c3isp.isi.api.restapi.types.xacml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.attributes.AttributesUtility;
import it.cnr.iit.xacml.attribute.AdditionalAttribute;
import it.cnr.iit.xacml.attribute.CATEGORY;

/**
 * Builder class for the request container
 * <p>
 *
 * </p>
 *
 * @author antonio
 *
 */
public class RequestBuilder {

	private RequestContainer requestContainer;
	private Logger log = Logger.getLogger(RequestBuilder.class.getName());

	public RequestBuilder() {
		requestContainer = new RequestContainer();
	}

	public void setResourceAttributes(Map<String, String> attributeMap) throws JsonProcessingException {
		AdditionalAttribute[] times = AttributesUtility.parseTime(attributeMap.get(AttributeIds.START_TIME),
				attributeMap.get(AttributeIds.END_TIME));
		addAttribute(AttributeIds.START_DATE, "date", times[0].getValue());
		addAttribute(AttributeIds.START_TIME, "time", times[1].getValue());
		addAttribute(AttributeIds.END_DATE, "date", times[2].getValue());
		addAttribute(AttributeIds.END_TIME, "time", times[3].getValue());
		addAttribute(AttributeIds.DSA_ID, "string", attributeMap.get(AttributeIds.DSA_ID));
		addAttribute(AttributeIds.RESOURCE_TYPE, "string", attributeMap.get(AttributeIds.RESOURCE_TYPE));
		addAttribute(AttributeIds.RESOURCE_OWNER, "string", attributeMap.get(AttributeIds.RESOURCE_OWNER));
	}

	public void setSubjectAttributes(Map<String, String> attributeMap) {
		addAttribute(AttributeIds.SUBJECT_ID, "string", attributeMap.get(AttributeIds.SUBJECT_ID));
		if (attributeMap.containsKey(AttributeIds.SUBJECT_ORGANIZATION)) {
			addAttribute(AttributeIds.SUBJECT_ORGANIZATION, "string",
					attributeMap.get(AttributeIds.SUBJECT_ORGANIZATION));
		}
		addAttribute(AttributeIds.ACCESS_PURPOSE, "string",
				Optional.ofNullable(attributeMap.get(AttributeIds.ACCESS_PURPOSE)).orElse("generic"));
		addAttribute(AttributeIds.AUTHENTICATION_TYPE, "string",
				Optional.ofNullable(attributeMap.get(AttributeIds.AUTHENTICATION_TYPE)).orElse("low"));
	}

	public void setActionAttributes(Map<String, String> attributeMap) {
		addAttribute(AttributeIds.ACTION_ID, "string", attributeMap.get(AttributeIds.ACTION_ID));
		addAttribute(AttributeIds.IAI_SESSION_ID, "string",
				Optional.ofNullable(attributeMap.get(AttributeIds.IAI_SESSION_ID)).orElse("000"));
	}

	public void setOtherAttributes(Map<String, String> attributeMap) {
		addAttribute(AttributeIds.AGGREGATION_OPERATION, "string",
				Optional.ofNullable(attributeMap.get(AttributeIds.AGGREGATION_OPERATION)).orElse("empty"));
	}

	private void addAttribute(String attributeId, String dataType, String value) {
		RequestAttributes requestAttributes = new RequestAttributes();
		requestAttributes.setAttributeId(attributeId);
		requestAttributes.setDatatype(dataType);
		requestAttributes.setValue(value);
		requestContainer.request.attribute.add(requestAttributes);
	}

	public RequestContainer build(Map<String, String> attributeMap) throws JsonProcessingException {
		if (attributeMap.containsKey(AttributeIds.SEARCH)) {
			addAttribute(AttributeIds.SEARCH, "string", attributeMap.get(AttributeIds.SEARCH));
			return requestContainer;
		}

		if (!attributeMap.get(AttributeIds.ACTION_ID).startsWith("Invoke")
				&& !attributeMap.get(AttributeIds.ACTION_ID).equals("read")) {
			setResourceAttributes(attributeMap);
		}

		setSubjectAttributes(attributeMap);
		setActionAttributes(attributeMap);
		setOtherAttributes(attributeMap);
		return requestContainer;
	}

	public static List<AdditionalAttribute> fromRequestAttributesToAdditionalAttribute(List<RequestAttributes> list) {
		List<AdditionalAttribute> resultList = new ArrayList<AdditionalAttribute>();

		for (RequestAttributes attr : list) {
			if (attr.getAttributeId().contains(":subject:")) {
				resultList.add(new AdditionalAttribute(CATEGORY.SUBJECT, attr.getAttributeId(), attr.getValue(),
						attr.getDatatype()));
			} else if (attr.getAttributeId().contains(":action:")) {
				resultList.add(new AdditionalAttribute(CATEGORY.ACTION, attr.getAttributeId(), attr.getValue(),
						attr.getDatatype()));
			} else if (attr.getAttributeId().contains(":resource:")) {
				resultList.add(new AdditionalAttribute(CATEGORY.RESOURCE, attr.getAttributeId(), attr.getValue(),
						attr.getDatatype()));
			}
		}
		return resultList;
	}

}
package eu.c3isp.isi.api.restapi.types.xacml;

import java.io.IOException;
import java.io.StringWriter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "RequestContainer", description = "Mapping request attributes in a XACML-like fashon")
// @XmlRootElement(name ="Request")
public class RequestContainer {

	// @XmlElement(name = "Request", nillable = false, required = true)
	@JsonProperty("Request")
	RequestElement request;

	@ApiModelProperty(allowEmptyValue = false, required = true, name = "Request")
	public RequestElement getRequest() {
		return request;
	}

	public void setRequest(RequestElement requestElement) {
		this.request = requestElement;
	}

	public RequestContainer() {

		request = new RequestElement();

	}

	/**
	 * serches for an attributeValue in a {@link RequestContainer}
	 * 
	 * @param attributeName
	 * @return a String with the attributeValue or <code>null</code> otherwise
	 */
	public String search(String attributeName) {
		if (request == null) {
			return null;
		}
		if (request.getAttributes().size() < 1) {
			return null;
		}
		for (RequestAttributes attr : request.getAttributes()) {
			if (attr.getAttributeId().compareToIgnoreCase(attributeName) == 0) {
				return attr.getValue();
			}
		}
		return null;
	}

	@Override
	public String toString() {
		StringWriter toReturn = new StringWriter();
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(toReturn, this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return toReturn.toString();
	}

//	public String returnXMLString() {
//		RequestType requestType = new RequestType();
//		HashMap<String, AttributesType> map = new HashMap<>();
//		for (RequestAttributes requestAttributes : request.getAttributes()) {
//			AttributesType attributesType = new AttributesType();
//			String category = getCategoryFromAttributeId(requestAttributes);
//			attributesType.setCategory(category);
//			AttributeType attributeType = new AttributeType();
//			attributeType.setAttributeId(requestAttributes.attributeId);
//			attributeType.setIncludeInResult(false);
//			AttributeValueType attributeValueType = new AttributeValueType();
//			attributeValueType.setDataType(DataType.STRING.toString());
//			attributeValueType.getContent().add(requestAttributes.value);
//			attributeType.getAttributeValue().add(attributeValueType);
//			attributesType.getAttribute().add(attributeType);
//			if (!map.containsKey(category)) {
//				map.put(category, attributesType);
//			} else {
//				map.get(category).getAttribute().add(attributeType);
//			}
//		}
//		for (Map.Entry<String, AttributesType> entry : map.entrySet()) {
//			requestType.getAttributes().add(entry.getValue());
//		}
//		try {
//			return JAXBUtility.marshalToString(RequestType.class, requestType, "Request", JAXBUtility.SCHEMA);
//		} catch (Exception e) {
//			return null;
//		}
//
//	}
//
//	private String getCategoryFromAttributeId(RequestAttributes requestAttribute) {
//		if (requestAttribute.attributeId.contains("subject")) {
//			return Category.SUBJECT.toString();
//		} else if (requestAttribute.attributeId.contains("resource")) {
//			return Category.RESOURCE.toString();
//		} else if (requestAttribute.attributeId.contains("action")) {
//			return Category.ACTION.toString();
//		} else if (requestAttribute.attributeId.contains("environment")) {
//			return Category.ENVIRONMENT.toString();
//		}
//		return "category";
//	}

	public void setAttributeById(String id, String value) {
		getRequest().getAttributes().stream().filter(el -> el.getAttributeId().equals(id)).findFirst().get()
				.setValue(value);
	}

	public RequestAttributes getAttributeById(String id) {
		return getRequest().getAttributes().stream().filter(el -> el.getAttributeId().equals(id)).findFirst().get();
	}
}
package eu.c3isp.isi.api.restapi.types;

public class ChartParam {
  private String dpoId;
  private String type;

  public String getDpoId() {
    return dpoId;
  }

  public void setDpoId(String dpoId) {
    this.dpoId = dpoId;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

}

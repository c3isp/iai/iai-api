package it.cnr.iit.xacml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.att.research.xacml.api.Attribute;
import com.att.research.xacml.api.Identifier;
import com.att.research.xacml.api.Request;
import com.att.research.xacml.api.RequestAttributes;
import com.att.research.xacml.std.StdMutableRequestAttributes;

import eu.c3isp.isi.api.restapi.types.xacml.XacmlUtils;

@Component
public class XACMLUtils {

	private XACMLUtils() {
	}

	public List<RequestAttributes> mergeRequests(Request partialRequest, Request request) {
		Map<Identifier, StdMutableRequestAttributes> map = partialRequest.getRequestAttributes().stream()
				.collect(Collectors.toMap(RequestAttributes::getCategory, StdMutableRequestAttributes::new));

		for (RequestAttributes attributes : request.getRequestAttributes()) {
			StdMutableRequestAttributes otherAttrs = map.get(attributes.getCategory());
			for (Attribute attr : attributes.getAttributes()) {
				otherAttrs.add(attr);
			}
		}
		return new ArrayList<>(map.values());
	}

	public String preparePolicy(String policy) {
		policy = policy.replaceAll("dsa-status:\\w+", "dsa-status").replaceAll("dsa-version:\\w+", "dsa-version")
				.replaceAll("subject:subject-id:\\w+", "subject:subject-id")
				.replaceAll("subject:subject-organisation:\\w+", "subject:subject-organisation")
				.replaceAll("subject:subject-role:\\w+", "subject:subject-role")
				.replaceAll("subject:subject-ismemberof:\\w+", "subject:subject-ismemberof")
				.replaceAll("subject:subject-country:\\w+", "subject:subject-country")
				.replaceAll("subject:access-purpose:\\w+", "subject:access-purpose")
				.replaceAll("action:action-id:\\w+", "action:action-id")
				.replaceAll("resource:resource-type:\\w+", "resource:resource-type")
				.replaceAll("resource:resource-owner:\\w+", "resource:resource-owner")
				.replaceAll("resource:resource-data-end-time:\\w+", "resource:data-end-time")
				.replaceAll("resource:resource-data-start-time:\\w+", "resource:data-start-time")
				.replaceAll("resource:resource-data-end-date:\\w+", "resource:data-end-date")
				.replaceAll("resource:resource-data-start-date:\\w+", "resource:data-start-date")
				.replaceAll("resource:dsa-id:\\w+", "resource:dsa-id")
				.replaceAll("environment:current-date:\\w+", "environment:current-date")
				.replaceAll("environment:current-time:\\w+", "environment:current-time");
		return patternToLower(policy, "Invoke\\w+");
	}

	public String preparePolicyForAuthorization(String policy) {
		String filteredPolicy = XacmlUtils.filterRuleIdPrefix(policy, "OBLIGATION").orElse(policy);
		return preparePolicy(filteredPolicy).replace("<Match", "<xacml:Match").replace("</Match", "</xacml:Match");
	}

	public String patternToLower(String data, String patternStr) {
		Pattern pattern = Pattern.compile(patternStr);
		Matcher m = pattern.matcher(data);
		while (m.find()) {
			String s = m.group(0);
			data = data.replaceAll(s, s.toLowerCase());
		}
		return data;
	}

}

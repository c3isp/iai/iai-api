package it.cnr.iit.iai.exceptions;

public class DpoException extends Exception {

	private static final long serialVersionUID = 7701473621210312394L;

	public DpoException(String msg) {
		super(msg);
	}

}

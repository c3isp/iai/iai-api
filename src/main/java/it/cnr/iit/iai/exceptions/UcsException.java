package it.cnr.iit.iai.exceptions;

public class UcsException extends Exception {

	private static final long serialVersionUID = 7701473621210312394L;

	private String additionalInfo;

	public UcsException(String msg) {
		super(msg);
	}

	public UcsException(String msg, String additionalInfo) {
		super(msg);
		this.additionalInfo = additionalInfo;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

}

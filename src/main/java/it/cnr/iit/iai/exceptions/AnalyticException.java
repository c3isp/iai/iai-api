package it.cnr.iit.iai.exceptions;

public class AnalyticException extends Exception {

	private static final long serialVersionUID = -4335143595850991158L;

	public AnalyticException(String msg) {
		super(msg);
	}

}

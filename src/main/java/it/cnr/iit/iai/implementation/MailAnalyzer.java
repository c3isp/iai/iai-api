//package it.cnr.iit.iai.implementation;
//
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.util.HashMap;
//import java.util.List;
//import java.util.concurrent.Callable;
//import java.util.logging.Logger;
//
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Component;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.util.UriComponentsBuilder;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.gson.Gson;
//
//import eu.c3isp.iai.subapi.types.AnalyticMetadata;
//import eu.c3isp.iai.subapi.types.DpoResponses;
//import eu.c3isp.iai.subapi.types.Paths;
//import eu.c3isp.isi.api.restapi.types.IAIParam;
//import eu.c3isp.isi.subapi.types.DataLakeBuffer;
//import eu.c3isp.isi.subapi.types.DpoInfo;
//import iit.cnr.it.classificationengine.basic.DataSet;
//import iit.cnr.it.classificationengine.basic.Element;
//import iit.cnr.it.classificationengine.basic.classifiers.RandomTreeForest;
//import iit.cnr.it.classificationengine.clusterer.cctree.CCTree;
//import iit.cnr.it.classificationengine.clusterer.cctree.Node;
//import it.cnr.iit.c3isp.mailiai.json.ClassForEmailID;
//import it.cnr.iit.c3isp.mailiai.json.ClassificationResult;
//import it.cnr.iit.iai.utility.ISIUtility;
//import it.cnr.iit.springswagger.restapi.impl.IAIImplUtils;
//import it.cnr.iit.springswagger.restapi.impl.UCSImplUtils;
//import it.cnr.iit.springswagger.restapi.types.UCSMessages;
//
//@Component
//public class MailAnalyzer implements Callable<DpoResponses> {
//
//	private RandomTreeForest<Integer> randomTreeForest;
//	private DataSet<Integer> dataset;
//
//	private String spamDetectUri;
//	private IAIParam parameter;
//	private AnalyticMetadata metadata;
//	private UtilsForAnalytics utilsForAnalytics;
//	private ISIUtility isiUtility;
//	private RestTemplate restTemplate;
//	private UCSMessages ucsMessages;
//	private UCSImplUtils ucsImplUtils;
//	private IAIImplUtils iaiImplUtils;
//
//	private final static Logger LOGGER = Logger.getLogger(MailAnalyzer.class.getName());
//
//	public MailAnalyzer() {
//	}
//
//	public MailAnalyzer instantiateMailAnalyzer(MailAnalyzer mailAnalyzer) {
//		try {
////			MailAnalyzer mailAnalyzer = new MailAnalyzer();
//			mailAnalyzer.dataset = utilsForAnalytics.retrieveData();
//			mailAnalyzer.randomTreeForest = new RandomTreeForest<>(mailAnalyzer.dataset, Integer.class);
//			mailAnalyzer.randomTreeForest.train();
//			return mailAnalyzer;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
//
//	@Override
//	public DpoResponses call() throws Exception {
//
//		if (parameter.getServiceName().equals("spamEmailClassify")) {
//			return classify();
//		} else if (parameter.getServiceName().equals("spamEmailClusterer")) {
//			return cluster();
//		} else if (parameter.getServiceName().equals("spamEmailDetect")) {
//			return spamEmailDetect();
//		} else {
//			return null;
//		}
//	}
//
//	// ---------------------------------------------------------------------------
//	// Classification
//	// ---------------------------------------------------------------------------
//
//	@Async
//	public DpoResponses classify() {
//		try {
//			// System.out.println("Sleeping for 30 sec");
//			// long startTime = System.currentTimeMillis() / 1000;
//			// System.out.println("start time=" + startTime);
//			// Thread.sleep(30000);
//			// System.out.println("Aweking...");
//			// long endTime = System.currentTimeMillis() / 1000;
//			// System.out.println("end time=" + endTime);
//			// System.out.println("time passed=" + (endTime - startTime));
//
//			List<String> dpoIds = isiUtility.searchDpo(parameter.getSearchCriteria());
//			if (dpoIds.isEmpty()) {
//				endAnalytic();
//				return iaiImplUtils.createDpoForError(UtilsForAnalytics.WARNING, UtilsForAnalytics.EMPTY_DLB,
//						metadata.getSessionId());
//			}
//			String dataLakeBufferUri = isiUtility.prepareData(metadata, dpoIds);
//			if (dataLakeBufferUri == null) {
//				endAnalytic();
//				return iaiImplUtils.createDpoForError(UtilsForAnalytics.WARNING, UtilsForAnalytics.NULL_DLB,
//						metadata.getSessionId());
//			}
//
//			DataLakeBuffer datalake = new ObjectMapper().readValue(dataLakeBufferUri, DataLakeBuffer.class);
//			HashMap<String, String> mailIdInDataset = new HashMap<>();
//			HashMap<String, HashMap<String, String>> mailAttributes = new HashMap<>();
//			DataSet<Integer> testdataset = utilsForAnalytics.buildDatasetFromEmailsAndFillTable(datalake,
//					mailIdInDataset, mailAttributes, parameter.getServiceParams().get("features_only"));
//
//			ClassificationResult classificationResult = classify(testdataset, mailIdInDataset, mailAttributes);
//			LOGGER.info(classificationResult.toString());
//			testdataset = null;
//
//			DpoResponses dpoResponses = utilsForAnalytics.loadResultInIsi(classificationResult, parameter,
//					metadata.getSessionId());
//			iaiImplUtils.fattenDpoResponses(dpoResponses, dpoIds, datalake);
//			endAnalytic();
//
//			return dpoResponses;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		endAnalytic();
//		return null;
//
//	}
//
//	private ClassificationResult classify(DataSet<Integer> testdataset, HashMap<String, String> mailIdInDataSet,
//			HashMap<String, HashMap<String, String>> mailAttributes) {
//		ClassificationResult classificationResult = new ClassificationResult();
//
//		for (int i = 0; i < testdataset.getNumberOfElements(); i++) {
//			Element<Integer> element = testdataset.getElement(i);
//			System.out.println("dpoID " + mailAttributes.get(Integer.toString(i)).get("dpo"));
//			System.out.println("filename " + mailAttributes.get(Integer.toString(i)).get("filename"));
//			System.out.println("recipient " + mailAttributes.get(Integer.toString(i)).get("recipient"));
//			System.out.println("subject " + mailAttributes.get(Integer.toString(i)).get("subject"));
//			System.out.println("sender " + mailAttributes.get(Integer.toString(i)).get("sender"));
//
//			ClassForEmailID classForEmailID = new ClassForEmailID();
//			element.removeFeature(0);
//			int classIndex = randomTreeForest.classify(element);
//			String className = dataset.getLabelDistinct().get(classIndex);
//			URI uri = null;
//			try {
//				uri = new URI(mailAttributes.get(Integer.toString(i)).get("dpo"));
//			} catch (URISyntaxException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			String[] segments = uri.getPath().split("/");
//			String idStr = segments[segments.length - 1].replace(".dpo", "");
//
//			classForEmailID.setMailId(idStr);
//			classForEmailID.setDestination(mailAttributes.get(Integer.toString(i)).get("recipient"));
//			classForEmailID.setSubject(mailAttributes.get(Integer.toString(i)).get("subject"));
//			classForEmailID.setSource(mailAttributes.get(Integer.toString(i)).get("sender"));
//			classForEmailID.setFilename(mailAttributes.get(Integer.toString(i)).get("filename"));
//
//			classForEmailID.setClassifiedAs(className);
//			LOGGER.info("Classification of email: " + element.toString() + " returned: " + className);
//			classificationResult.addClassifiedEmail(classForEmailID);
//		}
//
//		return classificationResult;
//	}
//
//	// ---------------------------------------------------------------------------
//	// Clustering
//	// ---------------------------------------------------------------------------
//	@Async
//	public DpoResponses cluster() {
//		try {
//			List<String> dpoIds = isiUtility.searchDpo(parameter.getSearchCriteria());
//			if (dpoIds.isEmpty()) {
//				endAnalytic();
//				return iaiImplUtils.createDpoForError(UtilsForAnalytics.WARNING, UtilsForAnalytics.EMPTY_DLB,
//						metadata.getSessionId());
//			}
//			String dataLakeBufferUri = isiUtility.prepareData(metadata, dpoIds);
//			if (dataLakeBufferUri == null) {
//				endAnalytic();
//				return iaiImplUtils.createDpoForError(UtilsForAnalytics.WARNING, UtilsForAnalytics.NULL_DLB,
//						metadata.getSessionId());
//			}
//
//			DataLakeBuffer datalake = new ObjectMapper().readValue(dataLakeBufferUri, DataLakeBuffer.class);
//			HashMap<String, String> mailIdInDataset = new HashMap<>();
//			HashMap<String, HashMap<String, String>> mailAttributes = new HashMap<>();
//
//			DataSet<Integer> testdataset = utilsForAnalytics.buildDatasetFromEmailsAndFillTable(datalake,
//					mailIdInDataset, mailAttributes, null);
//
//			testdataset.removeAttribute(0);
//			CCTree cctree = buildCCTree(parameter, testdataset);
//			if (cctree == null) {
//				System.out.println("cctree is null!!!!!!!!");
//			}
//			ClassificationResult classificationResult = new ClassificationResult();
//
//			cluster(cctree);
//			System.out.println("CLUSTERER completed");
//			System.out.println("ROOT node:" + cctree.getRoot().toString());
//			System.out.println("Clusters: " + cctree.getClusters().toString());
//
//			for (int i = 0; i < testdataset.getNumberOfElements(); i++) {
//				ClassForEmailID classForEmailID = new ClassForEmailID();
//
//				URI uri = new URI(mailAttributes.get(Integer.toString(i)).get("dpo"));
//				String[] segments = uri.getPath().split("/");
//				String idStr = segments[segments.length - 1];
//
//				classForEmailID.setMailId(idStr.split(".dpo")[0]);
//				classForEmailID.setDestination(mailAttributes.get(Integer.toString(i)).get("recipient"));
//				classForEmailID.setSubject(mailAttributes.get(Integer.toString(i)).get("subject"));
//				classForEmailID.setSource(mailAttributes.get(Integer.toString(i)).get("sender"));
//				classForEmailID.setFilename(mailAttributes.get(Integer.toString(i)).get("filename"));
//
//				Element<Integer> element = testdataset.getElement(i);
//				System.out.println("dpoID " + mailAttributes.get(Integer.toString(i)).get("dpo"));
//				System.out.println("filename " + mailAttributes.get(Integer.toString(i)).get("filename"));
//				System.out.println("recipient " + mailAttributes.get(Integer.toString(i)).get("recipient"));
//				System.out.println("subject " + mailAttributes.get(Integer.toString(i)).get("subject"));
//				System.out.println("sender " + mailAttributes.get(Integer.toString(i)).get("sender"));
//				// System.out.println("element " + element.toString());
//				for (Node node : cctree.getClusters()) {
//					String father = node.toString().split("Father: ")[1].split("leafId: ")[0];
//					String leafId = node.toString().split("leafId: ")[1].split("Attribute")[0];
//					String label = node.toString().split("Label: ")[1].split("------------")[0];
//					String[] labels = label.trim().split("\n");
//					for (String labelComputed : labels) {
//						if (labelComputed.contains(element.toString())) {
//							if (node.isLeaf()) {
//								classForEmailID.setPurity(String.valueOf(node.getPurity()));
//								classForEmailID.setLeafId(String.valueOf(node.getId()));
//								classificationResult.addClassifiedEmail(classForEmailID);
//								System.out.println("classForEmailID : " + new Gson().toJson(classForEmailID));
//								break;
//							}
//						}
//					}
//				}
//			}
//
//			LOGGER.info(classificationResult.toString());
//
//			DpoResponses dpoResponses = utilsForAnalytics.loadResultInIsi(classificationResult, parameter,
//					metadata.getSessionId());
//			iaiImplUtils.fattenDpoResponses(dpoResponses, dpoIds, datalake);
//			endAnalytic();
//
//			return dpoResponses;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		endAnalytic();
//		return null;
//	}
//
//	private CCTree buildCCTree(IAIParam params, DataSet<Integer> dataset) {
//		String purity = params.getServiceParams().get("purity");
//		String minElementsThreshold = params.getServiceParams().get("minElementsThreshold");
//		System.out.println("INFO: " + purity + "\t" + minElementsThreshold);
//		CCTree cctree = new CCTree(dataset);
//		double purityInt = -1;
//		if (purity != null) {
//			purityInt = Double.parseDouble(purity);
//			cctree.setPurityThreshold(purityInt);
//		}
//		int minElementsThresholdInt = -1;
//		if (minElementsThreshold != null) {
//			minElementsThresholdInt = Integer.parseInt(minElementsThreshold);
//			cctree.setMinElements(minElementsThresholdInt);
//		}
//		return cctree;
//	}
//
//	private ClassificationResult cluster(CCTree cctree) {
//		cctree.train();
//		// FIXME find a way to present results
//		return null;
//	}
//
//	// ---------------------------------------------------------------------------
//	// Spam Detect
//	// ---------------------------------------------------------------------------
//	@Async
//	public DpoResponses spamEmailDetect() {
//		try {
//
//			List<String> dpoIds = isiUtility.searchDpo(parameter.getSearchCriteria());
//			if (dpoIds.isEmpty()) {
//				endAnalytic();
//				return iaiImplUtils.createDpoForError(UtilsForAnalytics.WARNING, UtilsForAnalytics.EMPTY_DLB,
//						metadata.getSessionId());
//			}
//			String dataLakeBufferUri = isiUtility.prepareData(metadata, dpoIds);
//			if (dataLakeBufferUri == null) {
//				endAnalytic();
//				return iaiImplUtils.createDpoForError(UtilsForAnalytics.WARNING, UtilsForAnalytics.NULL_DLB,
//						metadata.getSessionId());
//			}
//
//			DataLakeBuffer datalake = new ObjectMapper().readValue(dataLakeBufferUri, DataLakeBuffer.class);
//			Paths paths = new Paths();
//			for (DpoInfo dpoInfo : datalake.getData()) {
//				paths.addPath(dpoInfo.getFile());
//			}
//
//			URI searchUri = UriComponentsBuilder.fromUriString(spamDetectUri).build().toUri();
//
//			HttpHeaders requestHeaders = new HttpHeaders();
//			requestHeaders.setContentType(MediaType.APPLICATION_JSON);
//			MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
//			body.addAll("paths", paths.getPaths());
//			HttpEntity<?> httpEntity = new HttpEntity<Object>(body, requestHeaders);
//			ResponseEntity<String> response = restTemplate.exchange(searchUri, HttpMethod.POST, httpEntity,
//					String.class);
//
//			LOGGER.info("SpamDetect result: " + response.getBody());
//
//			ClassificationResult classificationResult = ClassificationResult.createFromSpamDetect(response.getBody(),
//					datalake);
//			System.out.println("before calling loadResultInIsi, username=" + parameter.getSubjectId());
//			System.out.println("before calling loadResultInIsi, username=" + parameter.getSubjectId());
//			DpoResponses dpoResponses = utilsForAnalytics.loadResultInIsi(classificationResult, parameter,
//					metadata.getSessionId());
//			iaiImplUtils.fattenDpoResponses(dpoResponses, dpoIds, datalake);
//			endAnalytic();
//
//			return dpoResponses;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		endAnalytic();
//		return null;
//	}
//
//	private void endAnalytic() {
////		ucsImplUtils.performEndAccess();
////		isiUtility.releaseData(ucsMessages.getTryAccessResponse().getSessionId());
//	}
//
//	// Getter and setter
//
//	public IAIParam getParameter() {
//		return parameter;
//	}
//
//	public void setParameter(IAIParam parameter) {
//		this.parameter = parameter;
//	}
//
//	public AnalyticMetadata getMetadata() {
//		return metadata;
//	}
//
//	public void setMetadata(AnalyticMetadata metadata) {
//		this.metadata = metadata;
//	}
//
//	public UtilsForAnalytics getUtilsForAnalytics() {
//		return utilsForAnalytics;
//	}
//
//	public void setUtilsForAnalytics(UtilsForAnalytics utilsForAnalytics) {
//		this.utilsForAnalytics = utilsForAnalytics;
//	}
//
//	public ISIUtility getIsiUtility() {
//		return isiUtility;
//	}
//
//	public void setIsiUtility(ISIUtility isiUtility) {
//		this.isiUtility = isiUtility;
//	}
//
//	public RestTemplate getRestTemplate() {
//		return restTemplate;
//	}
//
//	public void setRestTemplate(RestTemplate restTemplate) {
//		this.restTemplate = restTemplate;
//	}
//
//	public String getSpamDetectUri() {
//		return spamDetectUri;
//	}
//
//	public void setSpamDetectUri(String spamDetectUri) {
//		this.spamDetectUri = spamDetectUri;
//	}
//
//	public UCSMessages getUcsMessages() {
//		return ucsMessages;
//	}
//
//	public void setUcsMessages(UCSMessages ucsMessages) {
//		this.ucsMessages = ucsMessages;
//	}
//
//	public UCSImplUtils getUcsImplUtils() {
//		return ucsImplUtils;
//	}
//
//	public void setUcsImplUtils(UCSImplUtils ucsImplUtils) {
//		this.ucsImplUtils = ucsImplUtils;
//	}
//
//	public IAIImplUtils getIaiImplUtils() {
//		return iaiImplUtils;
//	}
//
//	public void setIaiImplUtils(IAIImplUtils iaiImplUtils) {
//		this.iaiImplUtils = iaiImplUtils;
//	}
//
//}

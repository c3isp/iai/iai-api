package it.cnr.iit.iai.implementation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.isi.api.restapi.types.IAIParam;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import eu.c3isp.isi.subapi.types.DataLakeBuffer;
import eu.c3isp.isi.subapi.types.DpoInfo;
import it.cnr.iit.analytics.types.BruteForceParam;
import it.cnr.iit.analytics.types.ConnToMaliciousHostsParams;
import it.cnr.iit.analytics.types.DpoResponses;
import it.cnr.iit.analytics.types.ExploitParams;
import it.cnr.iit.analytics.types.SpamClassifyParameters;
import it.cnr.iit.analytics.types.SpamClustererParameters;
import it.cnr.iit.analytics.types.VulnerabilityAttackParams;
import it.cnr.iit.analytics.types.VulnerabilityParams;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;
import it.cnr.iit.common.utility.FileUtility;
import it.cnr.iit.database.tables.SessionTable;
import it.cnr.iit.iai.exceptions.AnalyticException;
import it.cnr.iit.springswagger.restapi.types.ComponentsWrapper;
import it.cnr.iit.ucs.message.endaccess.EndAccessResponseMessage;
import it.cnr.iit.utilty.Path;

@Service
/**
 * This class is in charge of executing the workflow of the analytics that
 * involve DGA.
 *
 * @author antonio
 *
 */
@Component
public class AwareAnalytics {

	// Find values in application-api-endpoint.properties
	@Value("${iai.api.detectDga}")
	private String detectDga;
	@Value("${iai.api.matchDga}")
	private String matchDga;
	@Value("${iai.api.bruteForceAttacksDetection}")
	private String bruteForceAttacksDetection;
	@Value("${iai.api.connToMaliciousHosts}")
	private String connToMaliciousHosts;
	@Value("${iai.api.findAttackingHosts}")
	private String findAttackingHosts;
	@Value("${iai.api.legacyAnalytic}")
	private String legacyAnalytic;
	@Value("${iai.api.spamDetect}")
	private String spamDetectUri;
	@Value("${iai.drillController.endpoint}")
	private String drillControllerEndpoint;
	@Value("${iai.api.threatAnalyzer}")
	private String threatAnalyzerUri;

	@Value("${iai.api.vulnerability.cveFromKeyword}")
	private String cveFromKeyword;
	@Value("${iai.api.vulnerability.exploitFromKeyword}")
	private String exploitFromKeyword;

	@Value("${suca.api.endpoint}")
	private String sucaEndpoint;
	@Value("${suca.api.endpoint.endAccess}")
	private String endAccessUri;

	// Find values in application-iai-implementation.properties
	@Value("${iai.implementation.error}")
	private String commonErrorString;
	@Value("${iai.implementation.datalakePath}")
	private String datalakePath;
	@Value("${iai.implementation.datalakeFile}")
	private String datalakeFile;

	@Autowired
	private ComponentsWrapper componentsWrapper;

	@Autowired
	private RestTemplate restTemplate;

	private static final Logger log = Logger.getLogger(AwareAnalytics.class.getName());

	public AwareAnalytics() {
	}

	@Async
	public CompletableFuture<DpoResponses> callC3ispAwareAnalytic(IAIParam params, RequestContainer requestContainer) {

		switch (params.getServiceName()) {
		case "findMalware":
		case "findVulnerability":
		case "correlateVulnerabilityAttack":
			return CompletableFuture.completedFuture(callAnalyticWithNoPrepareData(params, requestContainer));
		default:
			return CompletableFuture.completedFuture(callAnalyticByDefault(params, requestContainer));
		}
	}

	private DpoResponses callAnalyticWithNoPrepareData(IAIParam params, RequestContainer requestContainer) {
		AnalyticResult analyticResult;
		String sessionId = requestContainer.getAttributeById(AttributeIds.IAI_SESSION_ID).getValue();
		try {
			analyticResult = callCorrectAnalytic(params, null, null, sessionId);
			DpoResponses dpoResponses = componentsWrapper.getUtilsForAnalytics().loadResultInIsi(analyticResult, params,
					requestContainer.getAttributeById(AttributeIds.IAI_SESSION_ID).getValue());
			return dpoResponses;
		} catch (IOException | AnalyticException e) {
			e.printStackTrace();
			return componentsWrapper.getIaiImplUtils().createDpoForError(UtilsForAnalytics.ERROR, e.getMessage(),
					sessionId);
		}
	}

	private DpoResponses callAnalyticByDefault(IAIParam params, RequestContainer requestContainer) {
		String sessionId = requestContainer.getAttributeById(AttributeIds.IAI_SESSION_ID).getValue();

		try {
			List<String> dpoIds = componentsWrapper.getIsiUtility().searchDpo(params.getSearchCriteria());
			if (dpoIds.isEmpty()) {
				return componentsWrapper.getIaiImplUtils().createDpoForError(UtilsForAnalytics.WARNING,
						UtilsForAnalytics.EMPTY_DLB, sessionId);
			}

			String dataLakeBufferUri = componentsWrapper.getIsiUtility().prepareData(requestContainer, dpoIds);

			if (dataLakeBufferUri == null) {
				return componentsWrapper.getIaiImplUtils().createDpoForError(UtilsForAnalytics.WARNING,
						UtilsForAnalytics.NULL_DLB, sessionId);
			}

			DataLakeBuffer datalake = new ObjectMapper().readValue(dataLakeBufferUri, DataLakeBuffer.class);
			AnalyticResult analyticResult = getAnalyticResult(params, datalake, sessionId);
			if (analyticResult.getStatus().equals(AnalyticResult.REVOKE)) {
				return componentsWrapper.getIaiImplUtils().createDpoForError(UtilsForAnalytics.WARNING,
						UtilsForAnalytics.REVOKE, sessionId);
			}

			DpoResponses dpoResponses = componentsWrapper.getUtilsForAnalytics().loadResultInIsi(analyticResult, params,
					requestContainer.getAttributeById(AttributeIds.IAI_SESSION_ID).getValue());
			componentsWrapper.getIaiImplUtils().fattenDpoResponses(dpoResponses, dpoIds, datalake);

			log.severe("dpoResponses in CallAnalyticByDefault: " + new ObjectMapper().writeValueAsString(dpoResponses));

			if (analyticResult.getAdditionalInformations().containsKey("link")) {
				dpoResponses = new DpoResponses((String) analyticResult.getAdditionalInformations().get("link"));
			}

			return dpoResponses;

		} catch (JsonParseException e) {
			e.printStackTrace();
			return componentsWrapper.getIaiImplUtils().createDpoForError(UtilsForAnalytics.ERROR,
					e.getLocalizedMessage(), sessionId);
		} catch (IOException | AnalyticException e) {
			e.printStackTrace();
			return componentsWrapper.getIaiImplUtils().createDpoForError(UtilsForAnalytics.ERROR, e.getMessage(),
					sessionId);
		}

	}

	private AnalyticResult getAnalyticResult(IAIParam params, DataLakeBuffer datalake, String sessionId)
			throws AnalyticException {

		try {
			log.severe("Calling getAnalyticResult");
			String appendPath = datalakePath + UUID.randomUUID() + datalakeFile;
			for (DpoInfo dpoInfo : datalake.getData()) {
				String dataLakeBufferUri = dpoInfo.getFile();

				if (dataLakeBufferUri != null) {
					String content = FileUtils.readFileToString(new File(new URI(dataLakeBufferUri)));
					append(new File(appendPath), content);
				}
			}
			Path path = new Path(appendPath);
			return callCorrectAnalytic(params, datalake, path, sessionId);
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
			return new AnalyticResult(AnalyticResult.ERROR, "Something went wrong...");
		}
	}

	private ResponseEvent endAnalytic(String iaiSessionId) {
		SessionTable session = componentsWrapper.getCommonDatabase()
				.getFields(SessionTable.IAI_SESSION_ID_FIELD, iaiSessionId, SessionTable.class).get(0);
		EndAccessResponseMessage endResponse = componentsWrapper.getUcsClient()
				.performEndAccess(session.getUcsSessionId());
		log.severe("endAccessResponse is " + endResponse.getEvaluation().getResult().toUpperCase());
		componentsWrapper.getCommonDatabase().deleteEntry(session, SessionTable.class);
		return componentsWrapper.getIsiUtility().releaseData(iaiSessionId);
	}

	private AnalyticResult callCorrectAnalytic(IAIParam params, DataLakeBuffer datalake, Path path, String sessionId)
			throws IOException, AnalyticException {

		URI searchUri;
		ResponseEntity<String> response = null;

		log.severe("Calling " + params.getServiceName());

		String link = null;
		String serviceName = params.getServiceName();
		switch (serviceName) {

		case "detectDGA": {
			response = componentsWrapper.getAnalyticsCaller().detectDga(path, sessionId);
			log.severe("result from matchDGA: " + new ObjectMapper().writeValueAsString(response.getBody()));
			break;
		}
		case "matchDGA": {
			response = componentsWrapper.getAnalyticsCaller().matchDga(path, sessionId);
			log.severe("result from matchDGA: " + new ObjectMapper().writeValueAsString(response.getBody()));
			break;
		}
		case "bruteForceAttacksDetection": {
			BruteForceParam bruteForceParam = componentsWrapper.getPrepareParameters()
					.prepareBruteforceParameter(params, path.getPath());
			response = componentsWrapper.getAnalyticsCaller().bruteForceAttacksDetection(bruteForceParam, sessionId);
			break;
		}
		case "connToMaliciousHosts": {
			ConnToMaliciousHostsParams connToMaliciousHostsParams = componentsWrapper.getPrepareParameters()
					.prepareFHEParameters(params, datalake.getDatalake().getURI());
			response = componentsWrapper.getAnalyticsCaller().connToMaliciousHosts(connToMaliciousHostsParams);
			break;
		}
		case "findVulnerability": {
			VulnerabilityParams vulnerabilityParams = componentsWrapper.getPrepareParameters()
					.prepareVulnerabilityParameters(params);
			response = componentsWrapper.getAnalyticsCaller().findVulnerability(vulnerabilityParams);
			break;
		}
		case "findMalware": {
			ExploitParams exploitParams = componentsWrapper.getPrepareParameters().prepareExploitParameters(params);
			response = componentsWrapper.getAnalyticsCaller().findMalware(exploitParams);
			break;
		}
		case "findAttackingHosts": {
			List<String> lineList = Arrays.asList(datalake.getData()).stream().map(DpoInfo::getFile).map(Path::new)
					.map(componentsWrapper.getAnalyticsCaller()::findAttackingHosts)
					.filter(s -> s.getStatusCode().is2xxSuccessful()).map(ResponseEntity::getBody)
					.collect(Collectors.toList()).stream().collect(ArrayList::new, (a, s) -> {
						Arrays.asList(s.split("\n")).stream().forEach(e -> a.add(e));
					}, ArrayList::addAll);
			String result = lineList.stream().map(l -> l.split(": ")).filter(r -> r.length == 2)
					.collect(Collectors.toMap(r -> r[0], r -> Integer.parseInt(r[1]), (l, r) -> l + r)).entrySet()
					.stream().map(e -> e.getKey() + ": " + e.getValue()).collect(Collectors.joining(";"));
			result = "{\"resultFindAttackingHosts\": \"" + result + "\"}";
			response = ResponseEntity.ok(result);
			break;
		}
		case "legacyAnalytics": {
//			log.severe("welegacy");
			log.severe("\n\nIAIParams = " + new ObjectMapper().writeValueAsString(params) + "\n\n" + "datalake = "
					+ new ObjectMapper().writeValueAsString(datalake) + "\n\n" + "path = "
					+ new ObjectMapper().writeValueAsString(path));
			File f = new File(path.getPath());
//			log.severe("\n\ndecoded data = "
//					+ new String(Base64.getDecoder().decode(new String(Files.readAllBytes(Paths.get(f.getPath()))))));

			FileUtils.writeByteArrayToFile(f, Base64.getDecoder().decode(Files.readAllBytes(Paths.get(f.getPath()))));

			String oldExtension = FileUtility.getFileExtension(f.getPath());
			File f2 = new File(f.getPath().replaceAll(oldExtension, datalake.getExtension()));
			f2.createNewFile();
			Files.copy(Paths.get(f.getPath()), Paths.get(f2.getPath()), StandardCopyOption.REPLACE_EXISTING);
			f.delete();
			response = componentsWrapper.getAnalyticsCaller().legacyAnalytic(new FileSystemResource(f2.getPath()));
			link = legacyAnalytic.replaceAll("/api/guacamole/", "")
					+ new ObjectMapper().readTree(response.getBody()).get("link").asText();
			log.severe("\nwelink = " + link + "\n");
			break;
		}
		case "correlateVulnerabilityAttack": {
			VulnerabilityAttackParams vulnerabilityAttackParams = componentsWrapper.getPrepareParameters()
					.prepareVulnerabilityAttackParameters(params);
			response = componentsWrapper.getAnalyticsCaller().correlateVulnerabilityAttack(vulnerabilityAttackParams);
			break;
		}
		case "spamEmailClassify": {
			log.severe("datalake in awareAnalytics: " + new ObjectMapper().writeValueAsString(datalake));
			SpamClassifyParameters classifyParameters = componentsWrapper.getPrepareParameters()
					.prepareSpamClassifyParams(params, datalake);
			log.severe("SpamClassifyParameters in awareAnalytics: "
					+ new ObjectMapper().writeValueAsString(classifyParameters));
			response = componentsWrapper.getAnalyticsCaller().spamMailClassify(classifyParameters, sessionId);
			break;
		}
		case "spamEmailClusterer": {
			SpamClustererParameters clustererParameters = componentsWrapper.getPrepareParameters()
					.prepareSpamClustererParams(params, datalake);
			response = componentsWrapper.getAnalyticsCaller().spamMailClusterer(clustererParameters, sessionId);
			break;
		}
		default:
			log.severe("Call to a not existing Analytic");
			break;

		}

		ResponseEvent responseEvent = endAnalytic(sessionId);
		log.severe("response event in callCorrectAnalytic: " + new ObjectMapper().writeValueAsString(responseEvent));
		if (responseEvent.hasProperty("revoke")) {
			log.severe("if inside callCorrectAnalytic");
			return new AnalyticResult(AnalyticResult.REVOKE, UtilsForAnalytics.REVOKE);
		}

		AnalyticResult analyticResult = null;
		if (serviceName.contains("legacyAnalytics")) {
			analyticResult = new AnalyticResult();
			analyticResult.setStatus("ok");
			analyticResult.addAdditionalInformations("link", link);
		} else {
			checkForErrors(response);
			analyticResult = new ObjectMapper().readValue(response.getBody(), AnalyticResult.class);
		}

		log.severe("analyticResult in callCorrectAnalytic: " + new ObjectMapper().writeValueAsString(analyticResult));

		return analyticResult;

	}

	private void checkForErrors(ResponseEntity<String> response) throws IOException, AnalyticException {
		if (response.getStatusCode().is2xxSuccessful()) {
			AnalyticResult analyticResult = new ObjectMapper().readValue(response.getBody(), AnalyticResult.class);
			if (analyticResult.getStatus().equals(AnalyticResult.ERROR)) {
				throw new AnalyticException((String) analyticResult.getErrorMessage());
			}
		} else {
			throw new AnalyticException(response.getStatusCode().getReasonPhrase());
		}

	}

	private void append(File file, String content) {
		try (FileWriter fileWriter = new FileWriter(file, true)) {
			fileWriter.write(content);
			fileWriter.flush();
			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

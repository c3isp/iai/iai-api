package it.cnr.iit.iai.implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import eu.c3isp.isi.api.restapi.types.IAIParam;
import eu.c3isp.isi.subapi.types.DataLakeBuffer;
import it.cnr.iit.analytics.types.BruteForceParam;
import it.cnr.iit.analytics.types.ConnToMaliciousHostsParams;
import it.cnr.iit.analytics.types.ExploitParams;
import it.cnr.iit.analytics.types.SpamClassifyParameters;
import it.cnr.iit.analytics.types.SpamClustererParameters;
import it.cnr.iit.analytics.types.VulnerabilityAttackParams;
import it.cnr.iit.analytics.types.VulnerabilityParams;

@Component
public class PrepareParameters {

	@Value("${iai.drillController.cveDb}")
	private String cveDb;
	@Value("${iai.drillController.exploitDb}")
	private String exploitDb;
	@Value("${iai.drillController.edbCveDb}")
	private String edbCveDb;

	public PrepareParameters() {
	}

	public BruteForceParam prepareBruteforceParameter(IAIParam param, String dataLakeBufferUri) {
		BruteForceParam bruteForceParam = new BruteForceParam();
		bruteForceParam.setPath(dataLakeBufferUri);
		bruteForceParam.putMinFailures(param.getServiceParams().get("minFailures"));
		bruteForceParam.putMinFractionFailures(param.getServiceParams().get("minFractionFailures"));
		return bruteForceParam;
	}

	public ConnToMaliciousHostsParams prepareFHEParameters(IAIParam params, String dataLakeBufferUri) {
		ConnToMaliciousHostsParams fheParam = new ConnToMaliciousHostsParams();
		fheParam.setAnalysisMethod(params.getServiceParams().get("AnalysisMethod"));
		fheParam.setDatalake(dataLakeBufferUri);

		System.out.println(
				"params.getServiceParams().get(\"AnalysisMethod\")=" + params.getServiceParams().get("AnalysisMethod"));

		System.out.println("dataLakeBufferUri=" + dataLakeBufferUri);
		return fheParam;
	}

	public VulnerabilityParams prepareVulnerabilityParameters(IAIParam params) {
		VulnerabilityParams vulnerabilityParams = new VulnerabilityParams();
		vulnerabilityParams.setKeyword(params.getServiceParams().get("keyword"));
		vulnerabilityParams.setVulnerabilityPath(cveDb);
		return vulnerabilityParams;
	}

	public ExploitParams prepareExploitParameters(IAIParam params) {
		ExploitParams exploitParams = new ExploitParams();
		exploitParams.setKeyword(params.getServiceParams().get("keyword"));
		exploitParams.setExploitPath(exploitDb);
		exploitParams.setEdbCvePath(edbCveDb);
		return exploitParams;
	}

	public VulnerabilityAttackParams prepareVulnerabilityAttackParameters(IAIParam params) {
		VulnerabilityAttackParams vulnerabilityAttackParams = new VulnerabilityAttackParams();
		vulnerabilityAttackParams.setApplication(params.getServiceParams().get("application"));
		return vulnerabilityAttackParams;
	}

	public SpamClassifyParameters prepareSpamClassifyParams(IAIParam params, DataLakeBuffer dataLakeBufferUri) {
		SpamClassifyParameters classifyParameters = new SpamClassifyParameters();
		classifyParameters.setDataLakeBuffer(dataLakeBufferUri);
		classifyParameters.setFeaturesOnly(Boolean.parseBoolean(params.getServiceParams().get("features_only")));
		return classifyParameters;
	}

	public SpamClustererParameters prepareSpamClustererParams(IAIParam params, DataLakeBuffer dataLakeBufferUri) {
		SpamClustererParameters clustererParameters = new SpamClustererParameters();
		clustererParameters.setDataLakeBuffer(dataLakeBufferUri);
		clustererParameters.setPurity(Double.parseDouble(params.getServiceParams().get("purity")));
		clustererParameters
				.setMinElementsThreshold(Integer.parseInt(params.getServiceParams().get("minElementsThreshold")));
		return clustererParameters;

	}

}

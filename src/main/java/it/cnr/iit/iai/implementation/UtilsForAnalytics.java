package it.cnr.iit.iai.implementation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.isi.api.restapi.types.IAIParam;
import eu.c3isp.isi.api.restapi.types.xacml.RequestBuilder;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.analytics.types.DpoResponses;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.iai.exceptions.DpoException;
import it.cnr.iit.iai.utility.ISIUtility;
import it.cnr.iit.springswagger.restapi.impl.IAIImplUtils;

@Component
public class UtilsForAnalytics {

	private final static Logger log = Logger.getLogger(UtilsForAnalytics.class.getName());

	public static final String ALREADY_FINISHED = "The analytic has already finished";
	public static final String ALREADY_STOPPED = "The analytic was previously stopped";

	public static final String EMPTY_DLB = "No DPOs available for these search criteria";
	public static final String NULL_DLB = "You're not allowed to run the analytic";
	public static final String REVOKE = "A revoke has occurred during the analytic computation";
	public static final String WARNING = "WARNING";
	public static final String ERROR = "ERROR";

	@Autowired
	private IAIImplUtils iaiImplUtils;

	@Value("${iai.api.dsa.default}")
	private String defaultDsa;

	@Autowired
	private ISIUtility isiUtility;

	public UtilsForAnalytics() {
	}

	public <T> DpoResponses loadResultInIsi(T analyticResult, IAIParam params, String sessionId) {
		try {
			System.out.println("params=" + new ObjectMapper().writeValueAsString(params));
			String resultFilename = "result_" + UUID.randomUUID();

			Map<String, String> additionalAttributes = new HashMap<String, String>();
			populateAdditionaAttributeMap(additionalAttributes, params);

			RequestContainer requestContainer = new RequestBuilder().build(additionalAttributes);
			String response = isiUtility.uploadResultToDpo(analyticResult.toString(), requestContainer, resultFilename);
			System.out.println("returnedString: " + response);
			DpoResponses dpoResponses = iaiImplUtils.createDpoWithResult(response, sessionId);
			return dpoResponses;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private void populateAdditionaAttributeMap(Map<String, String> additionalAttributes, IAIParam params)
			throws DpoException {
		additionalAttributes.put(AttributeIds.SUBJECT_ID,
				Optional.of(params.getSubjectId()).orElseThrow(() -> new DpoException("Missing attribute "
						+ AttributeIds.SUBJECT_ID + " during the creation of the analytic result dpo")));
		additionalAttributes.put(AttributeIds.ACTION_ID, "create");
		additionalAttributes.put(AttributeIds.DSA_ID, defaultDsa);
		additionalAttributes.put(AttributeIds.RESOURCE_TYPE, params.getServiceName() + "-" + UUID.randomUUID());
		additionalAttributes.put(AttributeIds.START_TIME, getCurrentDate(true));
		additionalAttributes.put(AttributeIds.END_TIME, getCurrentDate(false));
		additionalAttributes.put(AttributeIds.RESOURCE_OWNER, params.getAdditionalAttribute().get("organisation"));
	}

	private String getCurrentDate(boolean endTime) {
		Date date;
		if (!endTime) {
			date = new Date(System.currentTimeMillis());
		} else {
			long twoYearsInMillis = 2 * 365 * 24 * 60 * 60 * 1000;
			date = new Date(System.currentTimeMillis() + twoYearsInMillis);
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
		String formattedDate = formatter.format(date);
		return formattedDate;
	}

}

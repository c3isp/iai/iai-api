package it.cnr.iit.iai.utility;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;

import it.cnr.iit.analytics.types.BruteForceParam;
import it.cnr.iit.analytics.types.ConnToMaliciousHostsParams;
import it.cnr.iit.analytics.types.ExploitParams;
import it.cnr.iit.analytics.types.SpamClassifyParameters;
import it.cnr.iit.analytics.types.SpamClustererParameters;
import it.cnr.iit.analytics.types.VulnerabilityAttackParams;
import it.cnr.iit.analytics.types.VulnerabilityParams;
import it.cnr.iit.utilty.Path;

@Component
public class AnalyticsCaller {

	@Value("${iai.api.detectDga}")
	private String detectDga;
	@Value("${iai.api.matchDga}")
	private String matchDga;
	@Value("${iai.api.bruteForceAttacksDetection}")
	private String bruteForceAttacksDetection;
	@Value("${iai.api.connToMaliciousHosts}")
	private String connToMaliciousHosts;
	@Value("${iai.api.findAttackingHosts}")
	private String findAttackingHosts;
	@Value("${iai.api.legacyAnalytics}")
	private String legacyAnalytics;
	@Value("${iai.api.spamDetect}")
	private String spamDetectUri;
	@Value("${iai.drillController.endpoint}")
	private String drillControllerEndpoint;
	@Value("${iai.api.threatAnalyzer}")
	private String threatAnalyzerUri;
	@Value("${iai.api.vulnerability.cveFromKeyword}")
	private String cveFromKeyword;
	@Value("${iai.api.vulnerability.exploitFromKeyword}")
	private String exploitFromKeyword;
	@Value("${iai.api.predictAttackTrend}")
	private String predictAttackTrendUri;
	@Value("${iai.api.spamEmailClassify}")
	private String spamEmailClassifyUri;
	@Value("${iai.api.spamEmailClusterer}")
	private String spamEmailClustererUri;

	@Value("${iai.api.legacyAnalytic}")
	private String legacyAnalyticUri;

	@Autowired
	private RestTemplate restTemplate;

	URI analyticEndpoint;
	ResponseEntity<String> response = null;

	public ResponseEntity<String> detectDga(Path path, String sessionId) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(detectDga).path("/").path(sessionId).build().toUri();
		return restTemplate.postForEntity(analyticEndpoint, path, String.class);
	}

	public ResponseEntity<String> matchDga(Path path, String sessionId) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(matchDga).path("/").path(sessionId).build().toUri();
		return restTemplate.postForEntity(analyticEndpoint, path, String.class);
	}

	public ResponseEntity<String> bruteForceAttacksDetection(BruteForceParam bruteForceParam, String sessionId) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(bruteForceAttacksDetection).path("/").path(sessionId)
				.build().toUri();
		System.out.println("calling " + analyticEndpoint);
		return restTemplate.postForEntity(analyticEndpoint, bruteForceParam, String.class);
	}

	public ResponseEntity<String> connToMaliciousHosts(ConnToMaliciousHostsParams connToMaliciousHostsParams) {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		analyticEndpoint = UriComponentsBuilder.fromUriString(connToMaliciousHosts + "/")
				.path(connToMaliciousHostsParams.getAnalysisMethod())
				.queryParam("vdlUrl", connToMaliciousHostsParams.getDatalake()).build().toUri();
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(body, requestHeaders);
		return restTemplate.exchange(analyticEndpoint, HttpMethod.POST, httpEntity, String.class);

	}

	public ResponseEntity<String> findVulnerability(VulnerabilityParams vulnerabilityParams) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(drillControllerEndpoint).path(cveFromKeyword)
				.queryParam("keyword", vulnerabilityParams.getKeyword())
				.queryParam("vulnerability_path", vulnerabilityParams.getVulnerabilityPath()).build().toUri();
		return restTemplate.getForEntity(analyticEndpoint, String.class);

	}

	public ResponseEntity<String> findMalware(ExploitParams exploitParams) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(drillControllerEndpoint).path(exploitFromKeyword)
				.queryParam("keyword", exploitParams.getKeyword())
				.queryParam("edb_cve_path", exploitParams.getEdbCvePath())
				.queryParam("exploit_path", exploitParams.getExploitPath()).build().toUri();
		return restTemplate.getForEntity(analyticEndpoint, String.class);

	}

	public ResponseEntity<String> findAttackingHosts(Path path) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(findAttackingHosts).build().toUri();
		return restTemplate.postForEntity(analyticEndpoint + "/", path, String.class);
	}

	public ResponseEntity<String> legacyAnalyticsSaturn(Path path) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(legacyAnalytics).build().toUri();
		return restTemplate.postForEntity(analyticEndpoint, path, String.class);
	}

	public ResponseEntity<String> correlateVulnerabilityAttack(VulnerabilityAttackParams vulnerabilityAttackParams) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(threatAnalyzerUri).build().toUri();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("applications", vulnerabilityAttackParams.getApplication());
		HttpEntity<?> httpEntity = new HttpEntity<Object>(body, requestHeaders);
		return restTemplate.exchange(analyticEndpoint, HttpMethod.POST, httpEntity, String.class);

	}

	public ResponseEntity<String> spamMailClassify(SpamClassifyParameters params, String sessionId)
			throws JsonProcessingException {
		analyticEndpoint = UriComponentsBuilder.fromUriString(spamEmailClassifyUri).path("/").path(sessionId).build()
				.toUri();
		return restTemplate.postForEntity(analyticEndpoint, params, String.class);
	}

	public ResponseEntity<String> spamMailClusterer(SpamClustererParameters params, String sessionId) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(spamEmailClustererUri).path("/").path(sessionId).build()
				.toUri();
		return restTemplate.postForEntity(analyticEndpoint, params, String.class);
	}

	public ResponseEntity<String> legacyAnalytic(FileSystemResource input) {
		analyticEndpoint = UriComponentsBuilder.fromUriString(legacyAnalyticUri).build().toUri();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("f", input);
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, requestHeaders);
		return restTemplate.postForEntity(analyticEndpoint, requestEntity, String.class);
	}

}

package it.cnr.iit.iai.utility;

import java.io.File;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.isi.api.restapi.types.SearchParams;
import eu.c3isp.isi.api.restapi.types.xacml.RequestBuilder;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.eventhandler.events.ResponseEvent;

@Component
public class ISIUtility {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${iai.isi.endpoint}")
	private String isiEndpoint;
	@Value("${isi.searchDpo.path}")
	private String searchDpoPath;
	@Value("${iai.isi.releaseData}")
	private String releaseDataMethod;
	@Value("${iai.isi.create}")
	private String createEndpoint;

	private final String METADATA_FORM = "input_metadata";
	private final String FILE_SUBMIT_FORM = "fileToSubmit";
	private final String DPO_SUFFIX = "/v1/dpo";

	private final static Logger log = Logger.getLogger(ISIUtility.class.getName());

	public ISIUtility() {
	}

	public List<String> searchDpo(SearchParams searchParams) {
		try {

			ObjectMapper mapper = new ObjectMapper();
			Map<String, String> attributeMap = new HashMap<>();
			attributeMap.put(AttributeIds.SEARCH, mapper.writeValueAsString(searchParams));
			RequestContainer requestContainer = new RequestBuilder().build(attributeMap);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			ObjectMapper objectMapper = new ObjectMapper();
			String search = objectMapper.writeValueAsString(searchParams);

			System.out.println("search: " + search);
			System.out.println(
					"prettyPrinter = " + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(requestContainer));
			System.err.println(headers);

			HttpEntity<RequestContainer> request = new HttpEntity<>(requestContainer, headers);

			URI searchUri = UriComponentsBuilder.fromUriString(isiEndpoint).path(searchDpoPath).build().toUri();

			System.out.println("searchUri = " + searchUri);
			ResponseEntity<String[]> response = restTemplate.exchange(searchUri, HttpMethod.POST, request,
					String[].class);

			return Arrays.asList(response.getBody());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String prepareData(RequestContainer requestContainer, List<String> dpoIds) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add("X-c3isp-metadata", requestContainer.toString());
			String[] dpoArray = dpoIds.toArray(new String[dpoIds.size()]);
			HttpEntity<String[]> request = new HttpEntity<String[]>(dpoArray, headers);
			URI prepareDataUri = buildURI(requestContainer.getAttributeById(AttributeIds.ACTION_ID).getValue());

			log.severe("Calling prepareData:\n" + "URI: " + prepareDataUri.toString() + "\n"
					+ "X-c3isp-metadata (header): " + requestContainer.toString() + "\n" + "dpoIDs (body): ");

			dpoIds.stream().forEach(el -> log.severe(el));
			ResponseEntity<String> response = restTemplate.exchange(prepareDataUri, HttpMethod.POST, request,
					String.class);

			if (!response.getStatusCode().is2xxSuccessful()) {
				log.severe("Nothing got from PrepareData. Did you get a DENY?");
				return null;
			}

			log.severe("prepareData: " + response.getBody());
			return response.getBody();
		} catch (Exception e) {
			System.out.println("In the catch body");
			e.printStackTrace();

		}
		return null;

	}

	public ResponseEvent releaseData(String sessionId) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> request = new HttpEntity<>(null, headers);

		URI releaseDataUri = UriComponentsBuilder.fromUriString(isiEndpoint).path(releaseDataMethod)
				.path(sessionId + "/").build().toUri();

		log.severe("Calling " + releaseDataMethod.toString());
		ResponseEntity<ResponseEvent> response = restTemplate.exchange(releaseDataUri, HttpMethod.POST, request,
				ResponseEvent.class);

		if (response.getStatusCode().is2xxSuccessful()) {
			log.severe("Data associated with sessionId " + sessionId + " correctly released");
		}
		if (response.getStatusCode().is4xxClientError()) {
			log.severe("Wrong request sent");
		}
		if (response.getStatusCode().is5xxServerError()) {
			log.severe("Something went wrong...");
		}

		return response.getBody();
	}

	private URI buildURI(String analyticName) {
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(isiEndpoint).path("/v1/prepareData")
				.queryParam("serviceName", analyticName);

		switch (analyticName) {

		case "InvokeDetectDGA":
		case "InvokeMatchDGA":
		case "InvokeBruteForceAttacksDetection":
		case "InvokeFindAttackingHosts":
		case "read":
		case "InvokeConnToMaliciousHosts":
			log.info(analyticName + " format: " + "CEF");
			uriBuilder.queryParam("data_lake", "VDL").queryParam("type", "FS").queryParam("format", "CEF");
			break;
		case "InvokeSpamEmailClassify":
		case "InvokeSpamEmailClusterer":
		case "InvokeSpamEmailDetect":
			log.info(analyticName + " format: " + "EML");
			uriBuilder.queryParam("data_lake", "VDL").queryParam("type", "FS").queryParam("format", "EML");
			break;
		case "InvokeLegacyAnalyticsSaturn":
			log.info(analyticName + " format: " + "CEF");
			uriBuilder.queryParam("data_lake", "VDL").queryParam("type", "MYSQL").queryParam("format", "CEF");
		case "InvokeLegacyAnalytics":
			log.info(analyticName + " format: " + "CEF");
			uriBuilder.queryParam("data_lake", "VDL").queryParam("type", "FS").queryParam("format", "CEF");
			break;
		default:
			log.severe("Wrong serviceName!");
			break;
		}

		return uriBuilder.build().toUri();
	}

	public String uploadResultToDpo(String result, RequestContainer requestContainer, String filePath)
			throws Exception {
		File file = createFileFromString(filePath, result);
		FileSystemResource resource = new FileSystemResource(file);
		String dpoResponse = uploadSingleFile(resource, requestContainer);
		log.info("DPO RESPONSE: " + dpoResponse);
		return dpoResponse;
	}

	private File createFileFromString(String filePath, String content) throws Exception {
		File file = File.createTempFile(filePath, "tmp");
		file.deleteOnExit();
		FileUtils.forceDeleteOnExit(file);
		FileUtils.writeByteArrayToFile(file, content.getBytes(StandardCharsets.UTF_8));
		return file;
	}

	private String uploadSingleFile(FileSystemResource file, RequestContainer requestContainer) throws Exception {
		LinkedMultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add(FILE_SUBMIT_FORM, file);
		body.add(METADATA_FORM, requestContainer);
		RequestEntity<MultiValueMap<String, Object>> requestEntity;
		requestEntity = RequestEntity.post(new URI(isiEndpoint + DPO_SUFFIX)).contentType(MediaType.MULTIPART_FORM_DATA)
				.body(body);

		ResponseEntity<String> response = restTemplate.exchange(requestEntity, String.class);
		return response.getBody();
	}

	public String readDpo(String dpoId, RequestContainer requestContainer) {
		try {

			HttpHeaders headers = new HttpHeaders();
			// headers.add("Authorization", "Basic " + base64Creds);
			headers.add("X-c3isp-input_metadata", requestContainer.toString());

			URI searchUri = UriComponentsBuilder.fromUriString(isiEndpoint).path(createEndpoint).path(dpoId + "/")
					.build().toUri();

			HttpEntity<RequestContainer> request = new HttpEntity<>(requestContainer, headers);

			String result = restTemplate.exchange(searchUri, HttpMethod.GET, request, String.class).getBody();

			// ResponseEntity<String> response = restTemplate.exchange(searchUri,
			// HttpMethod.POST, request, String.class);

			// ResponseEntity<String> responseEntity = new ResponseEntity<>(
			// response.getBody(), response.getHeaders(), response.getStatusCode());
			// System.out.println(responseEntity.getBody());
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String moveDpo(String dpoId, String location, RequestContainer requestContainer) {
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.add("X-c3isp-input_metadata", requestContainer.toString());
			headers.add("X-c3isp-destination", location);

			URI searchUri = UriComponentsBuilder.fromUriString(isiEndpoint).path(createEndpoint).path(dpoId + "/")
					.build().toUri();

			System.out.println(searchUri.toString());
			HttpEntity<RequestContainer> request = new HttpEntity<>(requestContainer, headers);

			String result = restTemplate.exchange(searchUri, HttpMethod.GET, request, String.class).getBody();

			System.out.println(result);

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

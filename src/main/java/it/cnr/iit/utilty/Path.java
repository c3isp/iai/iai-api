package it.cnr.iit.utilty;

/**
 * Parameter to be used with DGA related operations
 * 
 * @author antonio
 *
 */
public class Path {
  private String path;

  public Path() {

  }

  public Path(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

}

package it.cnr.iit.utilty;

import java.util.ArrayList;

public class Paths {
  private ArrayList<String> paths = new ArrayList<>();

  public void addPath(String path) {
    if (path != null && path.length() > 0) {
      paths.add(path);
    }
  }

  public ArrayList<String> getPaths() {
    return paths;
  }

  public void setPaths(ArrayList<String> paths) {
    this.paths = paths;
  }

}

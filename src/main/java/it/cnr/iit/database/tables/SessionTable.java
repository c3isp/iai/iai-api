package it.cnr.iit.database.tables;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "iai_sessions")
public class SessionTable {
	public static final String UCS_SESSION_ID = "ucs_session_id";
	public static final String IAI_SESSION_ID_FIELD = "iai_session_id";
	public static final String STATUS = "status";

	@DatabaseField(columnName = UCS_SESSION_ID, id = true, canBeNull = false)
	private String ucsSessionId;

	@DatabaseField(columnName = IAI_SESSION_ID_FIELD, canBeNull = false)
	private String iaiSessionId;

	@DatabaseField(columnName = STATUS, canBeNull = false)
	private String status;

	public SessionTable() {
	}

	public SessionTable(String ucsSessionId, String iaiSessionId, String status) {
		this.ucsSessionId = ucsSessionId;
		this.iaiSessionId = iaiSessionId;
		this.status = status;
	}

	public String getUcsSessionId() {
		return ucsSessionId;
	}

	public void setUcsSessionId(String ucsSessionId) {
		this.ucsSessionId = ucsSessionId;
	}

	public String getIaiSessionId() {
		return iaiSessionId;
	}

	public void setIaiSessionId(String iaiSessionId) {
		this.iaiSessionId = iaiSessionId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}

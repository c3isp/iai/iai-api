package it.cnr.iit.analytics.types;

import java.util.concurrent.Future;

public class AnalyticStructure {

	private Future<DpoResponses> future;
	private String sessionId;
	private String analyticName;

	public AnalyticStructure(Future<DpoResponses> future, String sessionId, String analyticName) {
		this.future = future;
		this.sessionId = sessionId;
		this.analyticName = analyticName;
	}

	public Future<?> getFuture() {
		return future;
	}

	public void setFuture(Future<DpoResponses> future) {
		this.future = future;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getAnalyticName() {
		return analyticName;
	}

	public void setAnalyticName(String analyticName) {
		this.analyticName = analyticName;
	}

}

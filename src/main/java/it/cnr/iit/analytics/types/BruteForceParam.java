package it.cnr.iit.analytics.types;

public class BruteForceParam {
	private String path;
	private int minFailures;
	private double minFractionFailures;

	public int getMinFailures() {
		return minFailures;
	}

	public void setMinFailures(int minFailures) {
		this.minFailures = minFailures;
	}

	public double getMinFractionFailures() {
		return minFractionFailures;
	}

	public void setMinFractionFailures(double minFractionFailures) {
		this.minFractionFailures = minFractionFailures;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void putMinFractionFailures(String minFractionFailures) {
		double val = 0;
		try {
			val = Double.parseDouble(minFractionFailures);
		} catch (Exception e) {
			val = 0;
			e.printStackTrace();
		}
		setMinFractionFailures(val);

	}

	public void putMinFailures(String minFailures) {
		int integer = 0;
		try {
			integer = Integer.parseInt(minFailures);
		} catch (Exception e) {
			integer = 0;
			e.printStackTrace();
		}
		setMinFailures(integer);
	}

}

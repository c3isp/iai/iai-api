package it.cnr.iit.analytics.types;

import com.fasterxml.jackson.annotation.JsonProperty;

import eu.c3isp.isi.subapi.types.DataLakeBuffer;

public class SpamClustererParameters {

	@JsonProperty("dataLakeBuffer")
	private DataLakeBuffer dataLakeBuffer;
	@JsonProperty("purity")
	private double purity = 0.5;
	@JsonProperty("minElementsThreshold")
	private int minElementsThreshold = 1;

	public SpamClustererParameters() {

	}

	public DataLakeBuffer getDataLakeBuffer() {
		return dataLakeBuffer;
	}

	public void setDataLakeBuffer(DataLakeBuffer dataLakeBuffer) {
		this.dataLakeBuffer = dataLakeBuffer;
	}

	public double getPurity() {
		return purity;
	}

	public void setPurity(double purity) {
		this.purity = purity;
	}

	public int getMinElementsThreshold() {
		return minElementsThreshold;
	}

	public void setMinElementsThreshold(int minElementsThreshold) {
		this.minElementsThreshold = minElementsThreshold;
	}

}

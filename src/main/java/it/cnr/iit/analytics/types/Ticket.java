package it.cnr.iit.analytics.types;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Return value for long running operations
 *
 * @author antonio
 *
 */
public class Ticket {

	public static final String NOT_ISSUED = "NOT_ISSUED";
	public static final String SUCA_DENY = "You are not allowed to run this analytic";
	public static final String SUCA_PERMIT = "OK";
	public static final String SUCA_INDETERMINATE = "Your attributes are not sufficient for giving you permission to run this operation";
	public static final String SUCA_NOT_APPLICABLE = "An error occured in the evaluation of your attributes";
	public static final String SUCA_ERROR = "An error occured during the policy evaluation";

	public static final String TICKET_RETRIEVING_ISSUE = "Cannot retrieve the DPO from ticket";
	public static final String TICKET_NO_RESULTS = "The analytic produced no results";
	public static final String TICKET_STOPPED = "The analytic associated with this ticket has been stopped";
	public static final String TICKET_REVOKED = "The access to the data associated with this ticket has been revoked";
	public static final String TICKET_RUNNING = "The analytic associated with this ticket is still running";
	public static final String TICKET_ERROR = "Something went wrong...";

	@JsonProperty("value")
	private String value;
	@JsonProperty("message")
	private String message;

	public Ticket() {

	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

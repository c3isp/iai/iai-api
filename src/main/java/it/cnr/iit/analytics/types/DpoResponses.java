package it.cnr.iit.analytics.types;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DpoResponses {
	public static final String ERROR_KEY = "error";
	private String link;

	@JsonProperty("result")
	private List<Result> results = new ArrayList<Result>();

	public DpoResponses() {

	}

	public DpoResponses(String link) {
		this.link = link;
	}

	public List<Result> getResults() {
		return results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

	public String getLink() {
		return link;
	}

}

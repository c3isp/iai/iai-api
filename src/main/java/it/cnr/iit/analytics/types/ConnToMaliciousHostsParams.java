package it.cnr.iit.analytics.types;

public class ConnToMaliciousHostsParams {

	private String datalake;
	private String analysisMethod;

	public ConnToMaliciousHostsParams() {
	}

	public String getAnalysisMethod() {
		return analysisMethod;
	}

	public void setAnalysisMethod(String analysisMethod) {
		this.analysisMethod = analysisMethod;
	}

	public String getDatalake() {
		return datalake;
	}

	public void setDatalake(String datalake) {
		this.datalake = datalake;
	}

}

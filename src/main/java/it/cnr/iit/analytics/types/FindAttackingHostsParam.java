package it.cnr.iit.analytics.types;

public class FindAttackingHostsParam {

	private String path;

	public FindAttackingHostsParam() {
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

}

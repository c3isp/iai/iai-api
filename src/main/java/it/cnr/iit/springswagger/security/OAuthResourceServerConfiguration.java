package it.cnr.iit.springswagger.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class OAuthResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;

//	@Value("${ldap.userSearchBase}")
//	private String ldapUserSearchBase;
//
//	@Value("${ldap.userSearchFilter}")
//	private String ldapUserSearchFilter;
//
//	@Value("${ldap.url}")
//	private String ldapUrl;
//
//	@Value("${ldap.principal}")
//	private String ldapUser;
//
//	@Value("${ldap.password}")
//	private String ldapPassword;

	@Value("${ldap.use.starttls}")
	private boolean useStartTls;

	/**
	 * These paths are required for Swagger and so must not be protected
	 */
	String[] apiPath = { "/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
			"/swagger-ui.html", "/webjars/**", "/ucs/**" };

	/**
	 * Here we configure authentication for REST endpoints starting with '/v1/' path
	 */
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId("pilot-interface-generic");
	}


	@Override
	public void configure(HttpSecurity http) throws Exception {
		System.out.println("securityActivationStatus=" + securityActivationStatus);
		if (!securityActivationStatus) {
			http.authorizeRequests().anyRequest().permitAll();
		} else {
			http.anonymous().and().authorizeRequests().antMatchers(apiPath).permitAll().antMatchers("/v1/**")
					.authenticated();
		}
	}

//	@Bean
//	public LdapContextSource contextSource() {
//		LdapContextSource contextSource = new LdapContextSource();
//
//		contextSource.setUrl(ldapUrl);
//		contextSource.setBase(ldapUserSearchBase);
//		contextSource.setUserDn(ldapUser);
//		contextSource.setPassword(ldapPassword);
//
//		return contextSource;
//	}
//
//	@Bean
//	public LdapTemplate ldapTemplate() {
//		return new LdapTemplate(contextSource());
//	}
	// @Bean
	// public LdapContextSource ldapContextSource() {
	// LdapContextSource contextSource = new LdapContextSource();
	// contextSource.setUrl(ldapUrl);
	// contextSource.setBase(ldapUserSearchBase);
	// contextSource.setPooled(false);
	// if (useStartTls) {
	// DefaultTlsDirContextAuthenticationStrategy strategy = new
	// DefaultTlsDirContextAuthenticationStrategy();
	// strategy.setShutdownTlsGracefully(true);
	// contextSource.setAuthenticationStrategy(strategy);
	// }
	//
	// return contextSource;
	// }
}
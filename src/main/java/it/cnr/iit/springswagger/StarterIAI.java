/**
 * Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.springswagger;

import java.security.Principal;
import java.util.Arrays;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextListener;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.AuthorizationCodeGrantBuilder;
import springfox.documentation.builders.OAuthBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.service.TokenEndpoint;
import springfox.documentation.service.TokenRequestEndpoint;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This class is the main spring boot entry, it implements the initializer class
 * and provides a main method that starts the application
 */
@EnableAutoConfiguration
@SpringBootApplication
@EnableSwagger2
@EnableAsync
@ComponentScan("it.cnr.iit")
@ComponentScan("eu.c3isp")
public class StarterIAI extends SpringBootServletInitializer {

	@Value("${security.activation.status}")
	private boolean securityActivationStatus;
	@Value("${security.oauth2.client.clientId}")
	private String clientId;
	@Value("${security.oauth2.client.clientSecret}")
	private String clientSecret;

	@Value("${security.oauth2.client.accessTokenUri}")
	private String accessTokenUri;
	@Value("${security.oauth2.client.userAuthorizationUri}")
	private String userAuthorizationUri;

	@Value("${security.oauth2.client.scope}")
	private String scope;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;

	/**
	 * Spring boot method for configuring the current application, right now it
	 * automatically scans for interfaces annotated via spring boot methods in all
	 * sub classes
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(StarterIAI.class);
	}

	@Bean
	public Docket documentation() {
		Docket docket = new Docket(DocumentationType.SWAGGER_2);
		docket.apiInfo(metadata());
		docket.useDefaultResponseMessages(false);
		if (!securityActivationStatus) {
			return docket.select().paths(PathSelectors.regex("/v1/.*")).build().ignoredParameterTypes(Principal.class);
		} else {
			return docket.select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.regex("/v1/.*")).build()
					.securitySchemes(Arrays.asList(securityScheme())).securityContexts(Arrays.asList(securityContext()))
					.ignoredParameterTypes(Principal.class).apiInfo(metadata());
		}
	}

	public SecurityScheme securityScheme() {
		GrantType grantType = new AuthorizationCodeGrantBuilder()
				.tokenEndpoint(new TokenEndpoint(accessTokenUri, "access_token"))
				.tokenRequestEndpoint(new TokenRequestEndpoint(userAuthorizationUri, clientId, clientSecret)).build();
		SecurityScheme oauth = new OAuthBuilder().name("spring_oauth").grantTypes(Arrays.asList(grantType))
				.scopes(Arrays.asList(scopes())).build();
		return oauth;
	}

	@Bean
	public SecurityConfiguration security() {
		return SecurityConfigurationBuilder.builder().clientId(clientId).clientSecret(clientSecret).scopeSeparator(" ")
				.useBasicAuthenticationWithAccessCodeGrant(false).build();
	}

	/**
	 * Selector for the paths this security context applies to ("/v1/.*")
	 */
	private SecurityContext securityContext() {
		return SecurityContext.builder()
				.securityReferences(Arrays.asList(new SecurityReference("spring_oauth", scopes())))
				.forPaths(PathSelectors.regex("/v1/.*")).build();
	}

	/**
	 * OIDC scopes
	 */
	private AuthorizationScope[] scopes() {
		AuthorizationScope authorizationScope1 = new AuthorizationScope("openid", "OIDC");
		AuthorizationScope authorizationScope2 = new AuthorizationScope("c3isp_scope", "c3isp scope");
		AuthorizationScope[] authorizationScopes = new AuthorizationScope[2];
		authorizationScopes[0] = authorizationScope1;
		authorizationScopes[1] = authorizationScope2;
		return authorizationScopes;
	}

	/**
	 * Here we use the same key defined in the security scheme (basicAuth)
	 */
	// List<SecurityReference> defaultAuth() {
	// AuthorizationScope authorizationScope = new AuthorizationScope("global",
	// "accessEverything");
	// AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
	// authorizationScopes[0] = authorizationScope;
	// // return new ArrayList<SecurityReference>(Arrays.asList(new
	// // SecurityReference("mykey", authorizationScopes)));
	// return new ArrayList<SecurityReference>(
	// Arrays.asList(new SecurityReference("basicAuth", authorizationScopes)));
	// }

	/**
	 * it just tells swagger that no special configuration are requested
	 */
	@Bean
	public UiConfiguration uiConfig() {
		return new UiConfiguration("validatorUrl" // set url
		);
	}

	private ApiInfo metadata() {
		return new ApiInfoBuilder().title("iai-api").description("iai-api for C3ISP project").version("1.0")
				.contact(new Contact("Antonio La Marra", "", "antonio.lamarra@iit.cnr.it"))
				.contact(new Contact("Calogero Lo Bue", "", "calogero.lobue@iit.cnr.it")).build();
	}

	public static void main(String[] args) {
		SpringApplication.run(StarterIAI.class, args);
	}

	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
//		int timeout = 120 * 1000;
//		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
//		httpRequestFactory.setConnectionRequestTimeout(timeout);
//		httpRequestFactory.setConnectTimeout(timeout);
//		httpRequestFactory.setReadTimeout(timeout);
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(restUser, restPassword));
		return restTemplate;

		// return restTemplateBuilder.basicAuthorization(restUser, restPassword)
		// .setConnectTimeout(10000).setReadTimeout(10000).build();

		// RIABILITARE QUESTO QUI SOTTO PER FARLO FUNZIONARE. MA NON VERIFICA IL
		// TIMEOUT
//		return restTemplateBuilder.basicAuthentication(restUser, restPassword).build();
	}

	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		int timeout = 600 * 1000;
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		clientHttpRequestFactory.setConnectTimeout(timeout);
		clientHttpRequestFactory.setReadTimeout(timeout);
		clientHttpRequestFactory.setConnectionRequestTimeout(timeout);
		return clientHttpRequestFactory;
	}

	@Bean
	public TaskExecutor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(20);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("iai-api ");
		executor.initialize();
		return executor;
	}

	@PostConstruct
	public void init() {
		heartbeatUCS();
	}

	public void heartbeatUCS() {
//		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
//		executor.scheduleAtFixedRate(heartbeat, 10, 300, TimeUnit.SECONDS);
	}

}

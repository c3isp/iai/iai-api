package it.cnr.iit.springswagger.restapi.impl;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.isi.api.restapi.types.ChartParam;
import eu.c3isp.isi.api.restapi.types.xacml.RequestBuilder;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import it.cnr.iit.analytics.types.VulnerabilityParams;
import it.cnr.iit.c3isp.mailiai.json.ClassificationResult;
import it.cnr.iit.c3isp.mailiai.json.chart.BarChart;
import it.cnr.iit.c3isp.mailiai.json.chart.PieChart;
import it.cnr.iit.common.analytic.AnalyticResult;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.attributes.AttributesUtility;
import it.cnr.iit.iai.utility.ISIUtility;

@ApiModel(value = "IAI API", description = "These are the APIs to call in order to perform an analytic."
		+ " The returned dpo will not contain any information related to the graphical representation of the result")
@RestController
@RequestMapping("/v1")
public class IAIGraphicsAPI {
	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local path,
	 * it can be in the java classpath or set as an environment variable
	 */
	@Autowired
	private ISIUtility isiUtility;

	@RequestMapping(method = RequestMethod.POST, value = "convertSpamInBarChart", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "analytics that takes as input the result of the Spam email (more specifically the DPOID of the result of the analytics)"
			+ " analytics and returns the chart representing the email")
	public String generateBarChartForSpam(@RequestBody() String dpoID, @RequestParam String username) {
		try {
//			if (principal != null) {
//				OAuth2Authentication oauth = (OAuth2Authentication) principal;
//				principal = oauth.getUserAuthentication();
//			}
			System.out.println("called generateBarChartForSpam");
			Map<String, String> attributeMap = new HashMap<String, String>();
			attributeMap.put(AttributeIds.ACTION_ID, "read");
			attributeMap.put(AttributeIds.SUBJECT_ID, username);
			RequestContainer requestContainer = new RequestBuilder().build(attributeMap);
			System.out.println("after building the requestContainer in generateBarChartForSpam");

//			AnalyticMetadata metadata = AnalyticMetadata.createAnalyticMetadataForChart(authentication);
			String jsonAnalyticResult = isiUtility.readDpo(dpoID, requestContainer);
			System.out.println("jsonAnalyticResult in generateBarChartForSpam: " + jsonAnalyticResult);
			AnalyticResult analyticResult = new ObjectMapper().readValue(jsonAnalyticResult, AnalyticResult.class);
			String result = new String(analyticResult.getResultArray().get(0), StandardCharsets.UTF_8);
			System.out.println("in generateBarChartForSpam, result: " + result);
			ClassificationResult classificationResult = new ObjectMapper().readValue(result,
					ClassificationResult.class);
			BarChart barChart = BarChart.convertClassificationResultToBarChart(classificationResult);
			ArrayList<BarChart> list = new ArrayList<>();
			list.add(barChart);
			System.out.println(new ObjectMapper().writeValueAsString(barChart));
			return new ObjectMapper().writeValueAsString(list);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "convertSpamInPieChart", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "analytics that takes as input the result of the Spam email (more specifically the DPOID of the result of the analytics)"
			+ " analytics and returns the chart representing the email")
	public String generatePieChartForSpam(@RequestBody() String dpoID, @RequestParam String username) {
		try {
//			if (principal != null) {
//				OAuth2Authentication oauth = (OAuth2Authentication) principal;
//				principal = oauth.getUserAuthentication();
//			}

			System.out.println("called generatePieChartForSpam");
			Map<String, String> attributeMap = new HashMap<String, String>();
			attributeMap.put(AttributeIds.ACTION_ID, "read");
			attributeMap.put(AttributeIds.SUBJECT_ID, username);
			RequestContainer requestContainer = new RequestBuilder().build(attributeMap);

//			AnalyticMetadata metadata = AnalyticMetadata.createAnalyticMetadataForChart(authentication);
			String jsonAnalyticResult = isiUtility.readDpo(dpoID, requestContainer);
			AnalyticResult analyticResult = new ObjectMapper().readValue(jsonAnalyticResult, AnalyticResult.class);
			String result = new String(analyticResult.getResultArray().get(0), StandardCharsets.UTF_8);
			System.out.println("in generatePieChartForSpam, result: " + result);
			ClassificationResult classificationResult = new ObjectMapper().readValue(result,
					ClassificationResult.class);
			ArrayList<PieChart> pieChart = PieChart.convertClassificationResultToPieChart(classificationResult);
			String pieChartAsString = new ObjectMapper().writeValueAsString(pieChart);
			return pieChartAsString;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "convertExploitInChart", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "analytics that takes as input the result of the Spam email (more specifically the DPOID of the result of the analytics)"
			+ " analytics and returns the chart representing the email")
	public String generateChartForExploit(@RequestBody() ChartParam chartParam, @RequestParam String username) {
		try {
//			Authentication authentication = null;
//			if (principal != null) {
//				OAuth2Authentication oauth = (OAuth2Authentication) principal;
//				principal = oauth.getUserAuthentication();
//			}

			Map<String, String> attributeMap = new HashMap<String, String>();
			attributeMap.put(AttributeIds.ACTION_ID, "read");
			attributeMap.put(AttributeIds.SUBJECT_ID, username);
			RequestContainer requestContainer = new RequestBuilder().build(attributeMap);
//			AnalyticMetadata metadata = AnalyticMetadata.createAnalyticMetadataForChart(authentication);
			String jsonAnalyticResult = isiUtility.readDpo(chartParam.getDpoId(), requestContainer);
			AnalyticResult analyticResult = new ObjectMapper().readValue(jsonAnalyticResult, AnalyticResult.class);
			String result = new String(analyticResult.getResultArray().get(0), StandardCharsets.UTF_8);
			System.out.println("in generateChartForExploit, result: " + result);
			VulnerabilityParams[] exploits = new ObjectMapper().readValue(result, VulnerabilityParams[].class);
			BarChart barChart = BarChart.convertClassificationResultToBarChart(exploits, chartParam.getType());
			System.out.println(new ObjectMapper().writeValueAsString(barChart));
			return new ObjectMapper().writeValueAsString(barChart.getValues());

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public String getDate(boolean isEndDate) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime date = isEndDate ? LocalDateTime.from(new Date().toInstant()).plusMonths(1) : LocalDateTime.now();
		LocalTime time = LocalTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
		return AttributesUtility.unparseTime(dtf.format(date).toString(), time.format(formatter).toString());
	}

}

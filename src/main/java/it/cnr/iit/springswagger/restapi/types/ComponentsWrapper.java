package it.cnr.iit.springswagger.restapi.types;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.clients.ucs.PEP;
import it.cnr.iit.clients.ucs.UcsClient;
import it.cnr.iit.database.common.CommonDatabase;
import it.cnr.iit.iai.implementation.PrepareParameters;
import it.cnr.iit.iai.implementation.UtilsForAnalytics;
import it.cnr.iit.iai.utility.AnalyticsCaller;
import it.cnr.iit.iai.utility.ISIUtility;
import it.cnr.iit.iai.utility.TicketManager;
import it.cnr.iit.springswagger.restapi.impl.IAIGenericApi;
import it.cnr.iit.springswagger.restapi.impl.IAIImplUtils;
import it.cnr.iit.ucs.core.UCSCoreServiceBuilder;
import it.cnr.iit.ucs.pdp.PolicyDecisionPoint;
import it.cnr.iit.ucs.properties.UCSProperties;
import it.cnr.iit.ucs.ucs.UCSInterface;
import it.cnr.iit.xacml.XACMLUtils;

@Component
public class ComponentsWrapper {

	@Autowired
	private UcsClient ucsClient;

	@Autowired
	private UCSProperties properties;

	@Autowired
	private IAIGenericApi iaiServiceImplementation;

	@Autowired
	private XACMLUtils xacmlUtils;

	@Autowired
	private IAIImplUtils iaiImplUtils;

	@Autowired
	private TicketManager ticketManager;

	@Autowired
	private CommonDatabase commonDatabase;

	@Autowired
	private ISIUtility isiUtility;

	@Autowired
	private PrepareParameters prepareParameters;

	@Autowired
	private UtilsForAnalytics utilsForAnalytics;

	@Autowired
	private AnalyticsCaller analyticsCaller;

	private PEP pep;

	private UCSInterface ucs;

	private PolicyDecisionPoint pdp;

	@PostConstruct
	private void init() {
		pep = new PEP(properties.getPepList().get(0), this);
		ucs = new UCSCoreServiceBuilder(pep, pep.getProperties()).setProperties(properties).build();
		pdp = new PolicyDecisionPoint(properties.getPolicyDecisionPoint());

	}

	public UcsClient getUcsClient() {
		return ucsClient;
	}

	public void setUcsClient(UcsClient ucsClient) {
		this.ucsClient = ucsClient;
	}

	public UCSProperties getProperties() {
		return properties;
	}

	public void setProperties(UCSProperties properties) {
		this.properties = properties;
	}

	public UCSInterface getUcs() {
		return ucs;
	}

	public void setUcs(UCSInterface ucs) {
		this.ucs = ucs;
	}

	public PolicyDecisionPoint getPdp() {
		return pdp;
	}

	public void setPdp(PolicyDecisionPoint pdp) {
		this.pdp = pdp;
	}

	public PEP getPep() {
		return pep;
	}

	public void setPep(PEP pep) {
		this.pep = pep;
	}

	public IAIGenericApi getIaiServiceImplementation() {
		return iaiServiceImplementation;
	}

	public void setIaiServiceImplementation(IAIGenericApi iaiServiceImplementation) {
		this.iaiServiceImplementation = iaiServiceImplementation;
	}

	public XACMLUtils getXacmlUtils() {
		return xacmlUtils;
	}

	public void setXacmlUtils(XACMLUtils xacmlUtils) {
		this.xacmlUtils = xacmlUtils;
	}

	public IAIImplUtils getIaiImplUtils() {
		return iaiImplUtils;
	}

	public void setIaiImplUtils(IAIImplUtils iaiImplUtils) {
		this.iaiImplUtils = iaiImplUtils;
	}

	public TicketManager getTicketManager() {
		return ticketManager;
	}

	public void setTicketManager(TicketManager ticketManager) {
		this.ticketManager = ticketManager;
	}

	public CommonDatabase getCommonDatabase() {
		return commonDatabase;
	}

	public void setCommonDatabase(CommonDatabase commonDatabase) {
		this.commonDatabase = commonDatabase;
	}

	public ISIUtility getIsiUtility() {
		return isiUtility;
	}

	public void setIsiUtility(ISIUtility isiUtility) {
		this.isiUtility = isiUtility;
	}

	public PrepareParameters getPrepareParameters() {
		return prepareParameters;
	}

	public void setPrepareParameters(PrepareParameters prepareParameters) {
		this.prepareParameters = prepareParameters;
	}

	public UtilsForAnalytics getUtilsForAnalytics() {
		return utilsForAnalytics;
	}

	public void setUtilsForAnalytics(UtilsForAnalytics utilsForAnalytics) {
		this.utilsForAnalytics = utilsForAnalytics;
	}

	public AnalyticsCaller getAnalyticsCaller() {
		return analyticsCaller;
	}

	public void setAnalyticsCaller(AnalyticsCaller analyticsCaller) {
		this.analyticsCaller = analyticsCaller;
	}

}

/**
 * Copyright 2017 Hewlett Packard Enterprise Development Company, L.P.
 */
package it.cnr.iit.springswagger.restapi.impl;

import java.io.IOException;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.c3isp.isi.api.restapi.types.IAIParam;
import eu.c3isp.isi.api.restapi.types.xacml.RequestBuilder;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import it.cnr.iit.analytics.types.DpoResponses;
import it.cnr.iit.analytics.types.Result;
import it.cnr.iit.analytics.types.Ticket;
import it.cnr.iit.clients.ucs.UcsUtils;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.common.types.DataManipulationObject;
import it.cnr.iit.database.tables.SessionTable;
import it.cnr.iit.iai.exceptions.UcsException;
import it.cnr.iit.iai.implementation.UtilsForAnalytics;
import it.cnr.iit.springswagger.restapi.types.ComponentsWrapper;
import it.cnr.iit.ucs.message.startaccess.StartAccessResponseMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessResponseMessage;
import it.cnr.iit.xacml.RequestGenerator;
import it.cnr.iit.xacml.RequestSerializationFactory;
import it.cnr.iit.xacml.attribute.AdditionalAttribute;
import oasis.names.tc.xacml.core.schema.wd_17.DecisionType;

@ApiModel(value = "IAI API", description = "These are the APIs to call in order to perform an analytic. The returned dpo will not contain any information related to the graphical representation of the result")
@RestController
@RequestMapping("/v1")
@Service
public class IAIGenericApi {

	@Value("${iai.pap.endpoint}")
	private String papEndpoint;
	@Value("${iai.pap.fetchPolicy}")
	private String fetchPolicy;
	@Value("${iai.pap.setPolicy}")
	private String setPolicy;

	@Value("${iai.isi.endpoint}")
	private String isiEndpoint;

	@Value("${iai.api.dsa.default}")
	private String defaultDsa;

	@Value("${prova.policy}")
	private String policyProva;

	@Autowired
	private ComponentsWrapper componentsWrapper;

//	@Autowired
//	private UCSImplUtils ucsImplUtils;

	private final static Logger log = Logger.getLogger(IAIGenericApi.class.getName());
	private static final Gson gson = new GsonBuilder().create();

	public IAIGenericApi() {
	}

	@RequestMapping(method = RequestMethod.POST, value = "/runAnalytics", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "runAnalytics: generic method to invoke any analytics available in the IAI API,"
			+ "	which takes	as input the name	of another analytics to	invoke and the set of parameters")

	public Ticket runAnalytics(@RequestBody() IAIParam param, Principal user) {

		try {
			param.setSubjectId(retrieveSubjectId(param, user));
			Authentication authentication = retrieveAuthentication(user);
			log.severe("username is " + param.getSubjectId());

			String analyticName = "Invoke" + param.getServiceName().substring(0, 1).toUpperCase()
					+ param.getServiceName().substring(1);
			log.severe("analyticName = " + analyticName);

			Map<String, String> attributeMap = new HashMap<>();
			attributeMap.put(AttributeIds.SUBJECT_ID, param.getSubjectId());
			attributeMap.put(AttributeIds.ACTION_ID, analyticName);
			attributeMap.put(AttributeIds.ACCESS_PURPOSE, param.getAdditionalAttribute().get("accessPurpose"));
			attributeMap.put(AttributeIds.IAI_SESSION_ID, UUID.randomUUID().toString());
			RequestContainer requestContainer = new RequestBuilder().build(attributeMap);
			List<AdditionalAttribute> attributeList = RequestBuilder
					.fromRequestAttributesToAdditionalAttribute(requestContainer.getRequest().getAttributes());
			attributeList.stream()
					.forEach(el -> log.severe("found attribute " + el.getName() + " with value " + el.getValue()));

			AdditionalAttribute[] addAttrArray = attributeList.toArray(new AdditionalAttribute[attributeList.size()]);

			String xacmlRequest = RequestSerializationFactory.newInstance().serialize(
					new RequestGenerator().createXACMLV3Request(true, addAttrArray),
					RequestSerializationFactory.SERIALIZATION_FORMAT.XML);

			String request = componentsWrapper.getXacmlUtils().patternToLower(xacmlRequest, "Invoke\\w+");
			String policy = componentsWrapper.getXacmlUtils().preparePolicyForAuthorization(policyProva);

			TryAccessResponseMessage tryResponse = componentsWrapper.getUcsClient().tryAccess(request, policy);
			String tryEvaluation = tryResponse.getEvaluation().getResult().toLowerCase();
			log.severe("TryAccess evaluation is " + tryEvaluation.toUpperCase());

			String outcome = tryResponse.getEvaluation().isDecision(DecisionType.PERMIT) ? "true" : "false";
			List<DataManipulationObject> dmos = null;
			if (outcome.equals("true")) {
				List<String> obligationRuleIdList = getObligationRuleIdList(request, policyProva);
				log.severe(
						"obligations extracted in iai: " + new ObjectMapper().writeValueAsString(obligationRuleIdList));
				String obligations = gson.toJson(obligationRuleIdList);
				if (!obligations.equals("[]")) {
					dmos = UcsUtils.performObligation(policyProva, obligations);
				}
			}

			if (tryEvaluation.equals("permit")) {
				componentsWrapper.getUcsClient().tryAccessSuccess(
						requestContainer.getAttributeById(AttributeIds.IAI_SESSION_ID).getValue(),
						tryResponse.getSessionId());
			} else {
				throw new UcsException("You are not allowed to run the analytic");
			}

			StartAccessResponseMessage startResponse = componentsWrapper.getUcsClient()
					.performStartAccess(tryResponse.getSessionId());
			String startEvaluation = startResponse.getEvaluation().getResult().toLowerCase();
			log.severe("StartAccess evaluation is " + startEvaluation.toUpperCase());

			if (startEvaluation.equals("permit")) {
				componentsWrapper.getUcsClient().startAccessSuccess(tryResponse.getSessionId());
				String ticket = componentsWrapper.getIaiImplUtils().runC3ispAwareAnalytic(param, requestContainer);
				return componentsWrapper.getIaiImplUtils().createTicket(ticket, Ticket.SUCA_PERMIT);
			} else {
				throw new UcsException("You are not allowed to run the analytic");
			}

		} catch (UcsException e) {
			log.severe(e.getMessage());
			if (!e.getAdditionalInfo().isEmpty()) {
				componentsWrapper.getUcsClient().cleanSession(e.getAdditionalInfo());
			}
			return componentsWrapper.getIaiImplUtils().createTicket(Ticket.NOT_ISSUED, e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Authentication retrieveAuthentication(Principal user) {
		Authentication authentication = null;
		if (user != null) {
			OAuth2Authentication oauth = (OAuth2Authentication) user;
			authentication = oauth.getUserAuthentication();
		}
		return authentication;
	}

	private String retrieveSubjectId(IAIParam param, Principal user) {
		if (user != null) {
			return user.getName();
		} else if (param.getSubjectId() != null) {
			return param.getSubjectId();
		}
		return "user";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/stopAnalytic")
	@ApiOperation(value = "This method take as input the ticket associated to the analytic that has to be stopped")
	public String stopAnalytic(@RequestBody String ticket) {
		log.severe("called stopAnalytic");

		componentsWrapper.getTicketManager().getTicketFromSessionIdHM().entrySet().stream()
				.forEach(entry -> log.severe("key = " + entry.getKey() + ", value = " + entry.getValue()));

		if (componentsWrapper.getTicketManager().getTicketFromSessionIdHM().containsKey(ticket)) {
			log.severe("calling stopFromUCS: a revoke arrived from DUCA");
			String sessionId = ticket;
			return componentsWrapper.getIaiImplUtils().stopFromUCS(sessionId);
		} else if (componentsWrapper.getTicketManager().getAnalyticStructureFromTicket().containsKey(ticket)) {
			log.severe("calling stopFromUser");
			return componentsWrapper.getIaiImplUtils().stopFromUser(ticket);
		} else if (componentsWrapper.getCommonDatabase().getField(SessionTable.UCS_SESSION_ID, ticket,
				SessionTable.class) != null) {
			log.severe("calling stopFromUCS: a revoke arrived from SUCA");
			SessionTable session = componentsWrapper.getCommonDatabase().getField(SessionTable.UCS_SESSION_ID, ticket,
					SessionTable.class);
			return componentsWrapper.getIaiImplUtils().stopFromUCS(session.getIaiSessionId());
		}

		return "Analytic already stopped";

	}

	@RequestMapping(method = RequestMethod.GET, value = "/getResponse/{ticket}/", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "retrieve the response from the ticket received")
	public String getResponse(@PathVariable("ticket") String ticket) {
		try {

			Future<DpoResponses> future = componentsWrapper.getTicketManager().getResult(ticket);

			if (future == null) {
				log.severe("future = null. returning: " + Ticket.TICKET_RETRIEVING_ISSUE);
				return new Gson().toJson(
						componentsWrapper.getIaiImplUtils().createTicket(ticket, Ticket.TICKET_RETRIEVING_ISSUE));
			}

			if (future.isDone()) {
				DpoResponses dpoResponses = future.get();
				if (dpoResponses == null || dpoResponses.getResults().size() == 0) {
					log.severe("future.get(). returning: " + Ticket.TICKET_NO_RESULTS);
					return new Gson()
							.toJson(componentsWrapper.getIaiImplUtils().createTicket(ticket, Ticket.TICKET_NO_RESULTS));
				}

				Result result = dpoResponses.getResults().get(0);

				if (result.getAdditionalProperties().containsKey(UtilsForAnalytics.ERROR)) {
					log.severe("future.get() with errors. returning: " + UtilsForAnalytics.ERROR);
					return new Gson().toJson(componentsWrapper.getIaiImplUtils().createTicket(ticket,
							result.getAdditionalProperties().get(UtilsForAnalytics.ERROR)));
				}
				return new Gson().toJson(result);
			}

			if (componentsWrapper.getTicketManager().getResult(ticket).isCancelled()) {
				log.info(Ticket.TICKET_REVOKED);
				return new Gson()
						.toJson(componentsWrapper.getIaiImplUtils().createTicket(ticket, Ticket.TICKET_REVOKED));
			}

			return new Gson().toJson(future.get(5, TimeUnit.SECONDS).getResults().get(0));

		} catch (TimeoutException te) {
			te.printStackTrace();
			return new Gson().toJson(componentsWrapper.getIaiImplUtils().createTicket(ticket, Ticket.TICKET_RUNNING));
		} catch (Exception e) {
			e.printStackTrace();
			log.severe("Error on retrieving result from ticket: " + e.getMessage());
			return new Gson().toJson(componentsWrapper.getIaiImplUtils().createTicket(ticket, Ticket.TICKET_ERROR));
		}
	}

	private List<String> getObligationRuleIdList(String request, String policy) {
		policy = UcsUtils.preparePolicyForObligation(policy);
		request = UcsUtils.prepareRequest(request);
		return UcsUtils.getTriggeredObligationRuleIdList(componentsWrapper.getUcs().enrichRequest(request), policy);
	}

}

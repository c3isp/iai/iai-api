package it.cnr.iit.springswagger.restapi.impl;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.c3isp.isi.api.restapi.types.IAIParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import it.cnr.iit.analytics.types.Ticket;
import it.cnr.iit.springswagger.restapi.types.ComponentsWrapper;

@ApiModel(value = "IAI API", description = "These are the APIs to call in order to perform an analytic. A ticket will be released and used to fetch analytics results.")
@RestController
@RequestMapping("/v1")
@Service
public class IAIApiList {

	@Autowired
	private ComponentsWrapper componentsWrapper;

	@RequestMapping(method = RequestMethod.POST, value = "/spamEmailClassify", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "analytics that takes as input a set of spam emails in eml format\r\n"
			+ " and returns the class assigned to each email, based on the spammer goal (e.g. phishing,\r\n"
			+ " advertisement, etc.)")
	public Ticket spamEmailClassify(@RequestBody() IAIParam param, Principal user) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/spamEmailClusterer", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Analytics that takes as input a set of spam emails and divides\r\n"
			+ " them in clusters based on structural similarity")
	public Ticket spamEmailClusterer(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/spamEmailDetect", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Takes as input one or more eml files and separates them in genuine emails (Ham) and unsolicited ones (Spam) sets")
	public Ticket spamEmailDetect(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/detectDGA", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "It takes domain-name logs and checks if they are DGA (Domain Generated\r\n"
			+ " Algorithm) using a third-party algorithm")
	public Ticket detectDGA(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/matchDGA", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "It takes domain-name logs and checks if they are DGA using a public\r\n"
			+ " source of known DGAs")
	public Ticket matchDGA(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/connToMaliciousHosts", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Paramater inputs are Analysis Method, Virtual Datalake Path containing FHE DPO-IDs.Each IP encrypted in FHE format is 32 ciphertexts, which are stored in one ZIP file corresponding to 1 DPO-IDDSA-ID is stored in DPO bundle. The analysis results for each IP will be packed into 1 zip file and send to DPOS for storing, then send back this DPO-ID")
	public Ticket connToMaliciousHosts(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	/*
	 * @RequestMapping(method = RequestMethod.POST, value = "/analyseDNSTraffic",
	 * produces = MediaType.APPLICATION_JSON_VALUE, consumes =
	 * MediaType.APPLICATION_JSON_VALUE)
	 *
	 * @ApiOperation(value =
	 * "This analytics that takes as input a set of DNS request logs in csv format and returns a list of domain names with suspicious network traffic behaviours (e.g. high frequency of requests)"
	 * ) public Ticket analyseDNSTraffic(@RequestBody() IAIParam param, Principal
	 * user) { return iaiServiceImplementation.runAnalytics(param, user); }
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/bruteForceAttacksDetection", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "takes SSH logs and highlights brute-force attacks considering the number of failed logins and the fraction of failed and accepted logins on the SSH protocol")
	public Ticket bruteForceAttacksDetection(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	// FIXME Change the return of this function!
	@RequestMapping(method = RequestMethod.POST, value = "/findAttackingHosts", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "takes SSH logs and highlights brute-force attacks considering the number of failed logins and the fraction of failed and accepted logins on the SSH protocol")
	public Ticket findAttackingHosts(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/findMalware", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Analytics that searches, in an archive stored in the DPOS, information about the malware related to the keyword.")
	public Ticket findMalware(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/findVulnerability", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Analytics that searches in an archive of CVE, stored in the DPOSthe keyword passed as a parameter")
	public Ticket findVulnerability(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/legacyAnalytics", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "run Legacy Analytics by creating and populating data into a Virtual Data Lake")
	public Ticket legacyAnalyticsSaturn(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/correlateVulnerabilityAttack", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Analytics that given an application name shows its known vulnerabilities with associated attack-patterns")
	public Ticket correlateVulnerabilityAttack(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/predictAttackTrend", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "nalyses a number of DPOs of interest containing detected attacks to a network and computes an ARIMA forecast about expected attacks in a future time frame")
	public Ticket predictAttackTrend(@RequestBody() IAIParam param, Principal user, String username) {
		return componentsWrapper.getIaiServiceImplementation().runAnalytics(param, user);
	}

}

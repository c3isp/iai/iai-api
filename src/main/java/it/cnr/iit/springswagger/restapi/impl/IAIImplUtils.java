package it.cnr.iit.springswagger.restapi.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.c3isp.isi.api.restapi.types.IAIParam;
import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
import eu.c3isp.isi.subapi.types.DataLakeBuffer;
import it.cnr.iit.analytics.types.AnalyticStructure;
import it.cnr.iit.analytics.types.DpoResponses;
import it.cnr.iit.analytics.types.Result;
import it.cnr.iit.analytics.types.Ticket;
import it.cnr.iit.common.attributes.AttributeIds;
import it.cnr.iit.iai.implementation.AwareAnalytics;
import it.cnr.iit.iai.utility.TicketManager;
//import it.cnr.iit.springswagger.restapi.types.UCSMessages;

@Component
public class IAIImplUtils {

	@Value("${prova.policy}")
	private String policy;
	@Value("${sme.id}")
	private String policySME;
	@Value("${sparta.id}")
	private String policySparta;
	@Value("${prova.id}")
	private String policyProvaId;
	@Value("${smeOnly.id}")
	private String policySMEOnly;

	@Value("${iai.api.spamDetect}")
	private String spamDetectUri;
	@Value("${policy.suca}")
	private String policySUCA;
	@Value("${policy.isp}")
	private String policyIsp;
	@Value("${policy.prova}")
	private String policyProva;

	@Value("${iai.api.bruteForceAttacksDetection}")
	private String bruteForceApi;
	@Value("${iai.api.matchDga}")
	private String matchDgaApi;
	@Value("${iai.api.detectDga}")
	private String detectDgaApi;
	@Value("${iai.api.spamEmailClassify}")
	private String spamEmailClassifyApi;

	@Value("${iai.api.stopAnalytic}")
	private String stopAnalyticApi;

	@Autowired
	private AwareAnalytics awareAnalytics;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	public TicketManager ticketManager;

	@Autowired
	public IAIImplUtils iaiImplUtils;

	private final static Logger log = Logger.getLogger(IAIImplUtils.class.getName());

	public IAIImplUtils() {
	}

	public String stopFromUCS(String sessionId) {
		String ticket = ticketManager.getTicketFromSessionIdHM().get(sessionId);
		log.severe("obtained ticket " + ticket + " from sessionId " + sessionId);
		String result = stopFromUser(ticket);
		if (result.contains("SUCCESS")) {
			ticketManager.getTicketFromSessionIdHM().remove(sessionId);
		}
		return result;
	}

	public String stopFromUser(String ticket) {
		try {

//			AnalyticStructure analyticStructure = ticketManager.getThreadFromTicketHM(ticket);
			AnalyticStructure analyticStructure = ticketManager.getAnalyticStructureFromTicket().get(ticket);

//			if (analyticStructure != null) {
//				return stopMailAnalytic(analyticStructure, ticket);
//			}

			log.severe("ticket in stopfromuser=" + ticket);

//			String sessionId = ticketManager.getSessionIdFromTicket().get(ticket);
//			String analyticName = ticketManager.getAnalyticNameFromTicket().get(ticket);

			String sessionId = analyticStructure.getSessionId();
			String analyticName = analyticStructure.getAnalyticName();

			callSpecificStopAnalytic(sessionId, analyticName);

//			Future<DpoResponses> future = ticketManager.getResult(ticket);
//
//			if (future.isDone()) {
//				return "INFO: The analytic has already finished";
//			}
//
//			if (future.isCancelled()) {
//				return "ERROR: The analytic was previously stopped";
//			}
//
//			ticketManager.ticketServed(ticket);
//
//			if (future.get() != null) {
//				List<Result> result = future.get().getResults();
//				// TODO: return the result
//			}

			return "SUCCESS: The analytic has been stopped";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private void callSpecificStopAnalytic(String sessionId, String analyticName) {

		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> request = new HttpEntity<>(sessionId, headers);
		URI uri;
		log.severe("callSpecificStopAnalytic, sessionId=" + sessionId);
		log.severe("callSpecificStopAnalytic, analyticName=" + analyticName);
		switch (analyticName) {
		case "bruteForceAttacksDetection": {
			uri = UriComponentsBuilder.fromUriString(bruteForceApi.replace("/detectBruteforce", ""))
					.path("/stopAnalytic").path("/").path(sessionId).build().toUri();
			log.severe("calling " + uri.toString());
			break;
		}
		case "matchDGA": {
			uri = UriComponentsBuilder.fromUriString(matchDgaApi.replace("/matchDGA", "")).path("/stopAnalytic")
					.path("/").path(sessionId).build().toUri();
			log.severe("calling " + uri.toString());
			break;
		}
		case "detectDGA": {
			uri = UriComponentsBuilder.fromUriString(detectDgaApi.replace("/detectDGA", "")).path("/stopAnalytic")
					.path("/").path(sessionId).build().toUri();
			log.severe("calling " + uri.toString());
			break;
		}
		case "spamEmailClassify": {
			uri = UriComponentsBuilder.fromUriString(spamEmailClassifyApi.replace("/spamEmailClassify", ""))
					.path("/stopAnalytic").path("/").path(sessionId).build().toUri();
			log.severe("calling " + uri.toString());
			break;
		}
		default:
			uri = null;
			break;
		}

		ResponseEntity<String> response = restTemplate.postForEntity(uri, request, String.class);

		log.severe("response from " + analyticName + " StopAnalytic = " + response.getBody());

	}

	public String runC3ispAwareAnalytic(IAIParam param, RequestContainer requestContainer) {
		String sessionId = requestContainer.getAttributeById(AttributeIds.IAI_SESSION_ID).getValue();
		log.severe("in runC3ispAwareAnalytic. sessionId = " + sessionId + ", serviceName = " + param.getServiceName());
		String ticket = ticketManager.generateUniqueTicket();
		log.severe("ticket in runC3ispAwareAnalytic =" + ticket);

		ticketManager.getTicketFromSessionIdHM().put(sessionId, ticket);
		CompletableFuture<DpoResponses> future = awareAnalytics.callC3ispAwareAnalytic(param, requestContainer);
		AnalyticStructure analyticStructure = new AnalyticStructure(future, sessionId, param.getServiceName());
		ticketManager.addAnalyticStructureFromTicket(ticket, analyticStructure);

		if (param.getServiceName().contains("legacyAnalytics")) {
			try {
				log.severe("wefuture.getLink = " + future.get().getLink());
				return future.get().getLink();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ticket;

	}

	public Ticket createTicket(String ticket, String message) {
		Ticket ticketResult = new Ticket();
		ticketResult.setValue(ticket);
		ticketResult.setMessage(message);
		System.out.println("ticket value is " + ticketResult.getValue());
		return ticketResult;
	}

	public String getCorrectPolicy(String analyticName) {

		System.out.println("Analytic Name: " + analyticName);
		switch (analyticName) {
		case "spamEmailClassify":
		case "spamEmailDetect":
		case "spamEmailClusterer":
		case "correlateVulnerabilityAttack":
		case "matchDGA":
		case "bruteForceAttacksDetection":
		case "connToMaliciousHosts":
		case "detectDGA":
		case "analyseDNSTraffic":
		case "findMalware":
		case "findVulnerability":
		case "findAttackingHosts":
		case "legacyAnalyticsSaturn":
		case "predictAttackTrend":
		case "securityOperationsExecutive":
			return policyProva;
//			return policySME;
		default:
			return "No policy found for analytic " + analyticName;
		}
	}

	public DpoResponses createDpoForError(String errorType, String errorMessage, String sessionId) {
		System.out.println("called createDpoForError: <" + errorType + ", " + errorMessage + ">");
		DpoResponses dpoResponses = new DpoResponses();
		List<Result> resultList = new ArrayList<>();
		Result result = new Result();
		HashMap<String, String> additionalProperties = new HashMap<>();
		additionalProperties.put(errorType, errorMessage);
		result.setSessionId(sessionId);
		result.setAdditionalProperties(additionalProperties);
		resultList.add(result);
		dpoResponses.setResults(resultList);
		return dpoResponses;
	}

	public DpoResponses createDpo(HashMap<String, String> additionalProperties, String sessionId) {
		DpoResponses dpoResponses = new DpoResponses();
		List<Result> resultList = new ArrayList<>();
		Result result = new Result();
		result.setSessionId(sessionId);
		result.setAdditionalProperties(additionalProperties);
		resultList.add(result);
		dpoResponses.setResults(resultList);
		return dpoResponses;

	}

	public DpoResponses createDpoWithResult(String analyticResult, String sessionId) {
		try {
			DpoResponses dpoResponses = new DpoResponses();
			JsonNode jsonNode = new ObjectMapper().readTree(analyticResult);
			Result result = null;
			if (jsonNode.has("result")) {
				String resultString = jsonNode.get("result").asText();
				result = new ObjectMapper().readValue(resultString, Result.class);
			} else {
				result = new ObjectMapper().readValue(analyticResult, Result.class);
			}
			result.setSessionId(sessionId);
			dpoResponses.getResults().add(result);
			return dpoResponses;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return createDpoForError("ERROR", "Json parse error", sessionId);
	}

	public void fattenDpoResponses(DpoResponses dpoResponses, List<String> dpoIds, DataLakeBuffer datalake) {
		try {
			dpoResponses.getResults().get(0).getAdditionalProperties().put("searchedDpos", dpoIds.toString());
			dpoResponses.getResults().get(0).getAdditionalProperties().put("permittedDpos",
					Arrays.asList(datalake.getData()).stream().map(f -> f.getDPO_id()).collect(Collectors.toList())
							.toString());
			log.info("Fatten DpoResponses=" + new ObjectMapper().writeValueAsString(dpoResponses));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}

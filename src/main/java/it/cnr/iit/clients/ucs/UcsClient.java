package it.cnr.iit.clients.ucs;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.cnr.iit.database.tables.SessionStatus;
import it.cnr.iit.database.tables.SessionTable;
import it.cnr.iit.iai.exceptions.UcsException;
import it.cnr.iit.springswagger.restapi.types.ComponentsWrapper;
import it.cnr.iit.ucs.message.endaccess.EndAccessMessage;
import it.cnr.iit.ucs.message.endaccess.EndAccessResponseMessage;
import it.cnr.iit.ucs.message.startaccess.StartAccessMessage;
import it.cnr.iit.ucs.message.startaccess.StartAccessResponseMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessMessage;
import it.cnr.iit.ucs.message.tryaccess.TryAccessResponseMessage;
import it.cnr.iit.ucs.properties.components.PepProperties;

@Component
public class UcsClient {

	@Autowired
	private ComponentsWrapper componentsWrapper;

	private static final Logger log = Logger.getLogger(UcsClient.class.getName());

	public TryAccessResponseMessage tryAccess(String request, String policy) {
		TryAccessMessage message = buildTryAccessMessage(request, policy);
		TryAccessResponseMessage response = (TryAccessResponseMessage) componentsWrapper.getUcs().tryAccess(message);
		return response;
	}

	public StartAccessResponseMessage startAccess(String sessionId) {
		StartAccessMessage message = buildStartAccessMessage(sessionId);
		StartAccessResponseMessage response = (StartAccessResponseMessage) componentsWrapper.getUcs()
				.startAccess(message);
		return response;
	}

	public EndAccessResponseMessage endAccess(String sessionId_ucs) {
		EndAccessMessage message = buildEndAccessMessage(sessionId_ucs);
		EndAccessResponseMessage response = (EndAccessResponseMessage) componentsWrapper.getUcs().endAccess(message);
		return response;
	}

	private TryAccessMessage buildTryAccessMessage(String request, String policy) {
		// FIXME getting only one pep here but we have a list instead
		PepProperties pepProperties = componentsWrapper.getProperties().getPepList().get(0);
		TryAccessMessage message = new TryAccessMessage(pepProperties.getId(), pepProperties.getUri());
		message.setRequest(request);
		message.setPolicy(policy);
		return message;
	}

	private StartAccessMessage buildStartAccessMessage(String sessionId) {
		// FIXME getting only one pep here but we have a list instead
		PepProperties pepProperties = componentsWrapper.getProperties().getPepList().get(0);
		StartAccessMessage message = new StartAccessMessage(pepProperties.getId(), pepProperties.getUri());
		message.setSessionId(sessionId);
		return message;
	}

	private EndAccessMessage buildEndAccessMessage(String sessionId) {
		// FIXME getting only one pep here but we have a list instead
		PepProperties pepProperties = componentsWrapper.getProperties().getPepList().get(0);
		EndAccessMessage message = new EndAccessMessage(pepProperties.getId(), pepProperties.getUri());
		message.setSessionId(sessionId);
		return message;
	}

	public void tryAccessSuccess(String iaiSessionId, String ucsSessionId) {
		SessionTable sessionTable = new SessionTable(ucsSessionId, iaiSessionId,
				SessionStatus.TRY_ACCESS_PERMIT.toString());
		componentsWrapper.getCommonDatabase().createOrUpdateEntry(sessionTable);
	}

	public StartAccessResponseMessage performStartAccess(String ucsSessionId) {
		StartAccessResponseMessage startResponse = componentsWrapper.getUcsClient().startAccess(ucsSessionId);
		log.severe("StartAccess evaluation is " + startResponse.getEvaluation().getResult());
		return startResponse;
	}

	public void startAccessSuccess(String ucsSessionId) throws UcsException {
		SessionTable session = Optional
				.ofNullable(componentsWrapper.getCommonDatabase().getField(SessionTable.UCS_SESSION_ID, ucsSessionId,
						SessionTable.class))
				.orElseThrow(() -> new UcsException(
						"An error occured while retrieving the session " + ucsSessionId + " from the database",
						ucsSessionId));

		session.setStatus(SessionStatus.START_ACCESS_PERMIT.toString());
		componentsWrapper.getCommonDatabase().createOrUpdateEntry(session);

	}

	public EndAccessResponseMessage performEndAccess(String ucsSessionId) {
		EndAccessResponseMessage endResponse = componentsWrapper.getUcsClient().endAccess(ucsSessionId);
		log.severe("EndAccess evaluation is " + endResponse.getEvaluation().getResult());
		return endResponse;
	}

	public void cleanSession(String additionalInfo) {
		SessionTable session = componentsWrapper.getCommonDatabase().getField(SessionTable.UCS_SESSION_ID,
				additionalInfo, SessionTable.class);
		List<SessionTable> sessionList = componentsWrapper.getCommonDatabase()
				.getFields(SessionTable.IAI_SESSION_ID_FIELD, session.getIaiSessionId(), SessionTable.class);
		sessionList.stream().forEach(row -> componentsWrapper.getCommonDatabase()
				.deleteEntry(SessionTable.UCS_SESSION_ID, additionalInfo, SessionTable.class));
	}

}

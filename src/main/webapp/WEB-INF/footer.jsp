<div class="card text-center border-0">
  <div class="card-header-footer">
    Footer
  </div>
  <div class="card-body dark-bg color-white">
  	<div class="row">
  		<div class="col-2">
  			<img class="footer-img" src="images/logo-cnr.png" height="60px">
  		</div>
  		<div class="col">
  			<p class="card-text">This project is funded under the European Union's Horizon 2020 Programme (H2020) - Grant Agreement n. 700294</p>
  		</div>
  		<div class="col-2">
  			<img class="footer-img" src="images/logo-iit.png" height="60px">
  		</div>
  	</div>
  </div>
</div>

<script src="js/bundle.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="js/index.js"></script>
<script src="js/validate_form.js"></script>
<script src="js/error.js"></script>
<script src="js/animations.js"></script>
<script src="js/ajax.js"></script>

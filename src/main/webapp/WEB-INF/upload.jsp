<div id="upload-page" class="main-content mt-3" style="display: none;">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">DATA MANAGER</a>
	
	<div class="row justify-content-center m-0">
		<div class="card mt-5 w-50 shadow p-3 mb-5 rounded card-style">
			<div class="card-header text-center ">
	    		Upload
			</div>
		    <div class="card-body">
		    
		    	<hr>
		    	
		    	<form id="data-manager-upload-form">
				    <div class="form-group row mb-5 mt-5">
				    
					    <div class="col-sm-8">
						    <div class="image-upload-wrap">
						    	<input id="uploader-single-file" name="uploader-single-file" class="file-upload-input" type='file' onchange="readURL(this);" readonly required />
					    		<div class="drag-text">
					      			<h4 id="file-name">Drag and drop<sup class="required-field">*</sup></h4>
					    		</div>
						    </div>						    
					    </div>
					    <div class="col-sm-3">
					    	<button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Select file</button>
					    </div>
					    
					</div>
					
					<hr>
					
					<div class="form-group row mt-5">
  				    	<label class="col-sm-3 col-form-label">Data Type<sup class="required-field">*</sup></label>
    					<div class="col-sm-5">
      						<select id="upload-data-type-select" name="event_type" class="custom-select mr-sm-2" aria-describedby="upload-data-type-help" required>
      							<option value="">Choose...</option>
						        <option value="ssh">ssh</option>
						        <option value="dns">dns</option>
						        <option value="e-mail">e-mail</option>
						        <option value="apache">apache</option>
						        <option value="netflow">netflow</option>
						    </select>
    					</div>
    					<div class="col-sm">
						    <small id="upload-data-type-help" class="text-muted">
						        Select the data type of the file you want to upload.
						    </small>
    					</div>
  					</div>
  					
  					<div class="form-group row mt-3">
  						<label class="col-sm-3 col-form-label">Start time<sup class="required-field">*</sup></label>
  						<div class="col-sm-5">
  							<input type="text" id="upload-start-time" name="start_time" class="form-control" aria-describedby="upload-start-time-help" placeholder="yyyy-mm-ddThh:mm:00:0Z" required>
  						</div>
  						<div class="col-sm">
						    <small id="upload-start-time-help" class="text-muted">
						    	Specify the event start time you are uploading.
						    </small>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-3">
  						<label class="col-sm-3 col-form-label">End time<sup class="required-field">*</sup></label>
  						<div class="col-sm-5">
  							<input type="text" id="upload-end-time" name="end_time" class="form-control" aria-describedby="upload-end-time-help" placeholder="yyyy-mm-ddThh:mm:00:0Z" required>
  						</div>
  						<div class="col-sm">
						    <small id="upload-end-time-help" class="text-muted">
						    	Specify the event end time you are uploading.
						    </small>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-3">
  				    	<label class="col-sm-3 col-form-label">Organization<sup class="required-field">*</sup></label>
    					<div class="col-sm-5">
      						<select class="custom-select mr-sm-2" id="upload-organization-select" name="organization" aria-describedby="upload-organization-help" required>
      							<option value="">Choose...</option>
						        <option value="CNR">isp@cnr</option>
						        <option value="CHINO">CHINO</option>
						    </select>
    					</div>
    					<div class="col-sm">
						    <small id="upload-organization-help" class="text-muted">
						        Select the organization name to whom you belong to.
						    </small>
    					</div>
  					</div>
  					
					<div id="select-div" class="form-group row mt-3 mb-5 error-subject" style="display: none">
  				    	<label class="col-sm-3 col-form-label">DSA ID<sup class="required-field">*</sup></label>
    					<div class="col-sm-5">
      						<select class="custom-select mr-sm-2" id="upload-dsa-id-select" name="dsa_id" aria-describedby="upload-dsa-help" required>
						    </select>
    					</div>
    					<div class="col-sm">
						    <small id="upload-dsa-help" class="text-muted">
						        Select the DSA ID.
						    </small>
    					</div>
  					</div>
  					
  					<hr>
  					
  					<div id="upload-error" class="row mt-5 justify-content-center alert alert-danger" style="display: none" role="alert"></div>
  					<div id="upload-info"  class="row mt-5 justify-content-center alert alert-info" style="display: none" role="alert"></div>
  					<div id="upload-success"  class="row mt-5 justify-content-center alert alert-success" style="display: none" role="alert"></div>
  					
  					<div class="row mt-5 justify-content-center">
  						<div class="col-3">
		  					<button type="submit" id="upload-form-submit" class="file-upload-btn">Submit</button>
  						</div>
  					</div>

		    	</form>
		    </div>
		</div>
	</div>
</div>

  



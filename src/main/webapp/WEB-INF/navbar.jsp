<nav class="navbar navbar-expand-lg fixed-top">
	<div class="d-flex justify-content-start">
		<a class="navbar-brand" href="index.jsp"> 
		  <img src="images/logo_main.png" class="img-fluid img-thumbnail" alt="C3ISP logo" height="48" width="48">
		</a>
	</div>
	
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
   		<span class="navbar-toggler-icon color-white"></span>
 		</button>

	<div class="collapse navbar-collapse" id="navbarText">
		<div class="d-flex ml-auto">
			<i class="fa fa-user color-white fa-2x ml-3"></i>
			<i class="fa fa-envelope color-white fa-2x ml-3"></i>
			<i class="fas fa-sign-in-alt color-white fa-2x ml-3"></i>
		</div>
	</div>
</nav>

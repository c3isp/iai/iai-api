<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <div class="sidebar-user-info">  		
    	<h2 class="pt-3">Welcome USER!</h2>
		<img src="images/avatar_man.png" class="sidebar-avatar-unlogged"></img>
    </div>
    
    <div id="accordion">
	    <a href="#" id="sb-home" class="btn btn-sm animated-button thar-four">HOME</a>
	    <a href="#" id="sb-analytic" class="btn btn-sm animated-button thar-four">ANALYTICS</a>
	    
	    <div id="headingTwo">
	        <h5 class="mb-0">
	      		<a href="#" class="btn btn-sm animated-button thar-four collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">DATA MANAGER</a>
	        </h5>
	    </div>		    
	    <div id="collapseTwo" class="collapse dark-bg" aria-labelledby="headingTwo" data-parent="#accordion">
        	<div id="sb-upload" class="text-right sidebar-row">
        		<div class="sidebar-icon">
			    	<i class="fas fa-file-upload color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<span class="color-white">Upload</span>
        		</div>	
        	</div>
        	<div id="sb-streaming" class="text-right sidebar-row">
        		<div class="sidebar-icon">
			    	<i class="fas fa-cloud-upload-alt color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<span class="color-white">Streaming</span>
        		</div>	
        	</div>
        	<div id="sb-search" class="text-right sidebar-row">
        		<div class="sidebar-icon">
			    	<i class="fas fa-search color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<span class="color-white">Search</span>
        		</div>	
        	</div>
        	<div id="sb-dsa" class="text-right sidebar-row">
        		<div class="sidebar-icon">
			    	<i class="fas fa-file-contract color-white"></i>
        		</div>
        		<div class="sidebar-element">
        			<span class="color-white">DSA</span>
        		</div>	
        	</div>
        </div>
    
</div>



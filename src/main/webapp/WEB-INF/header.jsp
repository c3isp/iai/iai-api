<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- glyph icons -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

<!-- sidebar -->
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Custom styles for this template-->
<link href="css/index.css" rel="stylesheet">
<link href="css/button.css" rel="stylesheet">
<link href="css/sidebar.css" rel="stylesheet">
<link href="css/data_manager.css" rel="stylesheet">
<link href="css/tables.css" rel="stylesheet">
<link href="css/animations.css" rel="stylesheet">

<!-- favicon -->
<link rel="icon" href="images/logo_main.png" type="images/png" sizes="16x16">

<title>Welcome on C3ISP!</title>
   
<div id="analytic-page" class="main-content mt-3" style="display: none">
	<a id="content-header" href="#" class="btn btn-sm animated-button thar-four">ANALYTICS</a>
	
	<div id="analytic-card" class="row justify-content-center m-0" >
		<div class="card mt-5 w-75 shadow p-3 mb-5 rounded card-style">
			<div class="card-header row no-gutters">
				<div class="col text-center">
		    		Analytic
				</div>
			</div>
			
		    <div class="card-body">
		    
		    	<hr>
		    	
		    	<form id="data-manager-analytic-form">
					<div class="form-group row mt-3">			
						<label class="col-sm-3 col-form-label">File location<sup class="required-field">*</sup></label>
						<div class="col-sm-2">
							<div class="form-check">
							    <input class="form-check-input" type="radio" name="radio-remote" id="radio-remote" value="remote" checked>
							    <label class="form-check-label" for="radio-remote">Remote</label>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-check">
							    <input class="form-check-input" type="radio" name="radio-local" id="radio-local" value="local">
							    <label class="form-check-label" for="radio-local">Local</label>
							</div>
						</div>
						<div class="col-sm">
						    <small class="text-muted">
						        Search your files locally or remotely.
						    </small>
    					</div>			
					</div>
					
					<hr>
					
					<div class="form-group row mt-4 mb-4">
  				    	<label class="col-sm-3 col-form-label">Data Type<sup class="required-field">*</sup></label>
    					<div class="col-sm-3">
      						<select class="custom-select" id="data-type" name="data-type" aria-describedby="data-type-help" required>
						        <option selected>Choose...</option>
						        <option value="cnr">.cef</option>
						        <option value="cert">.txt</option>
						        <option value="registro">.log</option>
						    </select>
    					</div>
    					<div class="col-sm-1"></div>
    					<div class="col-sm">
						    <small id="data-type-help" class="text-muted">
						        Select the data type of files you want to retrieve.
						    </small>
    					</div>
  					</div>
  					
  					<hr>
  					
  					<div class="form-group row mt-4">
  						<label class="col-sm-2 col-form-label">Organization name is</label>
  						<div class="col-sm-2">
  							<select class="custom-select" id="organization-select" name="organization-select" aria-describedby="organization-help">
						        <option value="">Choose...</option>
						        <option value="eq">equal</option>
						        <option value="ne">not equal</option>
						    </select>
  						</div>
  						<label class="col-sm-1 col-form-label text-center">to</label>
  						<div class="col-sm-3">
  							<input type="text" id="organization" name="organization" class="form-control" placeholder="isp@cnr, registro.it, ...">
  						</div>
  						<div class="col-sm">
						    <small id="organization-help" class="text-muted">
						    	Specify a condition selector related to the organization name.
						    </small>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-4">
  						<label for="analytic-organization" class="col-sm-2 col-form-label">DPO-ID is equal to</label>
  						<div class="col-sm-2">
  							<input type="text" id="dpo-id" name="dpo-id" class="form-control" placeholder="AV1234, ...">
  						</div>  					
  						<div class="col-sm-4"></div>	
  						<div class="col-sm">
						    <small id="organization-help" class="text-muted">
						    	Specify the DPO-ID.
						    </small>
  						</div>
  					</div>
  					
  					
  					<div class="form-group row mt-4">
  						<label class="col-sm-2 col-form-label">With a start time</label>
  						<div class="col-sm-3">
  							<select class="custom-select" id="start-time-select" name="start-time-select">
						        <option value="">Choose...</option>
						        <option value="le">less than</option>
						        <option value="lte">less than equal to</option>
						        <option value="eq">equal to</option>
						        <option value="gte">greater than equal to</option>
						        <option value="gt">greater then</option>
						        <option value="ne">not equal to</option>
						    </select>
  						</div>
  						<div class="col-sm-3">
  							<input type="text" id="start-time" name="start-time" class="form-control" aria-describedby="start-time-help" placeholder="yyyy-mm-ddThh:mm:00:0Z">
  						</div>
  						<div class="col-sm">
						    <small id="start-time-help" class="text-muted">
						    	Specify a condition selector related to the start time.
						    </small>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-4">
  						<label class="col-sm-2 col-form-label">With a end time</label>
  						<div class="col-sm-3">
  							<select class="custom-select" id="end-time-select" name="end-time-select">
						        <option value="">Choose...</option>
						        <option value="le">less than</option>
						        <option value="lte">less than equal to</option>
						        <option value="eq">equal to</option>
						        <option value="gte">greater than equal to</option>
						        <option value="gt">greater then</option>
						        <option value="ne">not equal to</option>
						    </select>
  						</div>
  						<div class="col-sm-3">
  							<input type="text" id="end-time" name="end-time" class="form-control" aria-describedby="end-time-help" placeholder="yyyy-mm-ddThh:mm:00:0Z">
  						</div>
  						<div class="col-sm">
						    <small id="end-time-help" class="text-muted">
						    	Specify a condition selector related to the end time.
						    </small>
  						</div>
  					</div>
  					
  					<div class="form-group row mt-4 mb-5">
  						<label class="col-sm-2 col-form-label">Security level is</label>
  						<div class="col-sm-2">
  							<select class="custom-select" id="security-select" name="security-select" aria-describedby="security-help">
						        <option value="">Choose...</option>
						        <option value="eq">equal</option>
						        <option value="ne">not equal</option>
						    </select>
  						</div>
  						<label class="col-sm-1 col-form-label text-center">to</label>
  						<div class="col-sm-2">
	  						<select class="custom-select" id="level-select" name="level-select" aria-describedby="security-help">
						        <option value="">Choose...</option>
						        <option value="INFORMATIONAL">Informational</option>
						        <option value="WARNING">Warning</option>
						        <option value="MINOR">Minor</option>
						        <option value="MAJOR">Major</option>
						        <option value="CRITICAL">Critical</option>
						    </select>
  						</div>
  						<div class="col-sm-1"></div>
  						<div class="col-sm">
						    <small id="decurity-help" class="text-muted">
						    	Specify a condition selector related to the organization name.
						    </small>
  						</div>
  					</div>
  					
  					<hr>
  					
  					
				    
  					<div class="row mt-4 justify-content-center">  						
  						<div class="col-3">
		  					<button id="submit" type="submit" class="file-upload-btn">Submit</button>
  							<label for="submit"><small>Press to submit</small></label>
  						</div>
  						<div class="col-3">
		  					<button id="and-analytic-button" type="submit" class="file-upload-btn">AND</button>
		  					<label for="and-analytic-button"><small>Press to add an exclusive search</small></label>
  						</div>
  						<div class="col-3">
		  					<button id="or-analytic-button" type="submit" class="file-upload-btn">OR</button>
		  					<label for="or-analytic-button"><small>Press to add a disjunctive search</small></label>
  						</div>
  					</div>
		    	</form>
		    </div>
		</div>
	</div>
	
	<div id="analytic-table" class="row justify-content-center m-0 mt-5" style="display: none;">
		<div class=col-2></div>
		<div class="col">
			<table class="table shadow">
			    <thead>
				    <tr>
					    <th scope="col">#</th>
					    <th scope="col">DPO-iD</th>
					    <th scope="col">Timestamp</th>
					    <th scope="col">Download</th>
				    </tr>
			    </thead>
			    <tbody>
				    <tr>
					    <td>1</td>
					    <td>Mark</td>
					    <td>Otto</td>
					    <td>@mdo</td>
				    </tr>
				    <tr>
					    <td>2</td>
					    <td>Jacob</td>
					    <td>Thornton</td>
					    <td>@fat</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
				    <tr>
					    <td>3</td>
					    <td>Larry</td>
					    <td>the Bird</td>
					    <td>@twitter</td>
				    </tr>
			    </tbody>
			</table>
		</div>
		<div class=col-2></div>
	</div>
	
	<div class="analytic-lateral-panel" style="display: none;">
		Search
	</div>
</div>

  



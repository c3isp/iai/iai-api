<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="build/nv.d3.css" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js" charset="utf-8"></script>
    <script src="build/nv.d3.js"></script>
    <script src="lib/stream_layers.js"></script>

    <style>
        text {
            font: 12px sans-serif;
        }
        svg {
            display: block;
        }
        html, body, #test1, svg {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
        }
    </style>
</head>
<body>

<div id="test1">
    <svg></svg>
</div>
	
<p id="demo"></p>

<script>

    //var test_data = stream_layers(3,128,.1).map(function(data, i) {
    var test_data = stream_layers(3,128,.1).map(function(data, i) {
        return {
            key: (i == 1) ? 'Non-stackable Stream' + i: 'Stream' + i,
            nonStackable: (i == 1),
            values: data
        };
    });

    console.log('td',test_data);

 document.getElementById("demo").innerHTML = test_data[0].values[0];

  test_data = [{"key":"CVE","nonStackable":false,"values":[{"x":"01/07/2018","y":474,"y0":"01/07/2018","y1":474},{"x":"01/08/2018","y":17,"y0":"01/08/2018","y1":17},{"x":"01/09/2018","y":53,"y0":"01/09/2018","y1":53},{"x":"01/10/2018","y":2,"y0":"01/10/2018","y1":2}]},{"key":"Botnet","nonStackable":false,"values":[{"x":"01/07/2018","y":30,"y0":"01/07/2018","y1":30},{"x":"01/08/2018","y":3,"y0":"01/08/2018","y1":3},{"x":"01/09/2018","y":71,"y0":"01/09/2018","y1":71},{"x":"01/10/2018","y":4,"y0":"01/10/2018","y1":4}]},{"key":"Trojan","nonStackable":false,"values":[{"x":"01/07/2018","y":133,"y0":"01/07/2018","y1":133},{"x":"01/08/2018","y":24,"y0":"01/08/2018","y1":24},{"x":"01/09/2018","y":85,"y0":"01/09/2018","y1":85},{"x":"01/10/2018","y":9,"y0":"01/10/2018","y1":9}]},{"key":"Ransomware","nonStackable":false,"values":[{"x":"01/07/2018","y":540,"y0":"01/07/2018","y1":540},{"x":"01/08/2018","y":33,"y0":"01/08/2018","y1":33},{"x":"01/09/2018","y":90,"y0":"01/09/2018","y1":90},{"x":"01/10/2018","y":54,"y0":"01/10/2018","y1":54}]}];


    nv.addGraph({
        generate: function() {
            var width = nv.utils.windowSize().width,
                height = nv.utils.windowSize().height;

            var chart = nv.models.multiBarChart()
                .width(width)
                .height(height)
                .stacked(true)
                ;

            chart.dispatch.on('renderEnd', function(){
                console.log('Render Complete');
            });

            var svg = d3.select('#test1 svg').datum(test_data);
            console.log('calling chart');
            svg.transition().duration(0).call(chart);

            return chart;
        },
        callback: function(graph) {
            nv.utils.windowResize(function() {
                var width = nv.utils.windowSize().width;
                var height = nv.utils.windowSize().height;
                graph.width(width).height(height);

                d3.select('#test1 svg')
                    .attr('width', width)
                    .attr('height', height)
                    .transition().duration(0)
                    .call(graph);

            });
        }
    });

</script>
</body>
</html>
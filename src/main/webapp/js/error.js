function decodeError(result,type,error_page){
	if(result.length == 0){
		appendError("No result found",error_page);
		return -1;
	}
	
	if(result == null || result === undefined){
		appendError("Null or undefined result. Fix the bug!",error_page);
		return -1;
	}
	
	if(result === "exception"){
		appendError("Something went wrong. Please retry again.",error_page);
		return -1;
	}
	
	if(result === "invalid"){
		appendError("Invalid "+type+" format",error_page);
		return -1;
	}
	
	if(result === "required"){
		appendError("You must fill all the required fields",error_page);
		return -1;
	}
	
	if(result === "2-required"){
		appendError("You must provide both condition selector and its input",error_page);
		return -1;
	}
	
	if(result === "no-condition-selector"){
		appendError("You must provide at most one condition selector and its input",error_page);
		return -1;
	}
	
	return 0;
}

function appendError(error,error_page){
	console.log(error);
	$(".error-subject").fadeOut();
	$("#"+error_page+"-error").fadeOut().text(error).fadeIn();
}
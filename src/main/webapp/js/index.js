//global var
var nsearch = 0;
var nand = 0;
var nor = 0;
var npanel = 0;
var isActualCardTmp = true;
var show_existing_card = false;
var show_tmp_card = true;


var protoSearchCard = {
		
	cardType : "search",
	id: "",
	formData: "",
	hasSearchPanel: false,
	hasOperatorPanel: false,
	visibility: "visible",
	tmp: true,
	operator: "none",
	sessionStorageId: "",
	
	goAway: function(callback){
		goAwaySearchCard(callback);
	},
	comeBack: function(callback){
		comeBackSearchCard(callback);
	},
	validateForm: function(form_data){
		return validateSearchForm(form_data);
	},
	restoreForm: function(){
		restoreFormValues(sessionStorage.getItem(this.sessionStorageId,this.formData));
	}
}

var protoUploadCard = {
		
	cardType : "upload",
	formData: "",
	filename: "",
	file: "",
	uuid: "",
	
	validateForm: function(){
		if(validateDateRegex("upload","start") == -1 || validateDateRegex("upload","end") == -1)
			return -1;
		else 
			return 0;
	}
}

var protopanel = {
	id: "",
	searchId: "",
	operator: "none", //[and,or,none]
	selected: false, //[true,false]
	visible: false //[true,false]
}

var cardArray = [];
var panelArray = [];

/************* SIDEBAR BUTTONS HANDLERS ********************/

$(function(){
	$("#sb-upload").click(function(){
		$("#home-page").fadeOut();
		$("#search-page").fadeOut();
		$("#analytic-page").fadeOut();
		$("#upload-page").delay(500).fadeIn();
	});
	
	$("#sb-search").click(function(){
		$("#home-card").fadeOut();
		$("#upload-page").fadeOut();
		$("#analytic-page").fadeOut();
		$("#search-page").delay(500).fadeIn();
		createFirstCard();
	});
	
	$("#sb-home").click(function(){
		$("#upload-page").fadeOut();
		$("#search-page").fadeOut();
		$("#analytic-page").fadeOut();
		$("#home-page").delay(500).fadeIn();
	});
	
	$("#sb-analytic").click(function(){
		$("#upload-page").fadeOut();
		$("#search-page").fadeOut();
		$("#home-page").fadeOut();
		$("#analytic-page").delay(500).fadeIn();
	});
});


/************* FORM HANDLERS ********************/

$(function(){
	$("#data-manager-upload-form").on("submit",function(form){
		form.preventDefault();
		
		var card = Object.create(protoUploadCard);
		
		if(card.validateForm() == -1)
			return;
		
		card.formData = $("#data-manager-upload-form").serialize();
		card.file = $("#uploader-single-file").get(0).files[0];
		card.filename = $("#uploader-single-file").get(0).files[0].name; 
		card.uuid = crypto.getRandomValues(new Uint32Array(4)).join('-');
		
		card.formData = card.formData.replace(/%3A/g,":");
		card.formData = card.formData.replace(/%2B/g,"+");
		
		var form_data = new FormData();
		form_data.append("file",card.file);
		
		var param_array = card.formData.split("&");
		
		var json = '{\n' +
			'"id": "' + card.uuid + card.filename + '",\n';
		
		for(var i=0; i<param_array.length; i++){
			var query_item = param_array[i];
			var item = query_item.split("=");
			var field = item[0];
			var value = item[1];
			
			if(i == param_array.length-1)
				json += '"'+field+'": '+'"'+value+'"\n';
			else
				json += '"'+field+'": '+'"'+value+'",\n';
		}
		json += "}";
		
		uploadSingleFileMetadata(json);
		uploadSingleFile(form_data);
		
		console.log("upload submitted");
	});
	
	$("#data-manager-search-form").on("submit",function(form){
		form.preventDefault();
		
		var card = cardArray[cardArray.length-1];

		console.log("card.id="+card.id);
		
		card.formData = $("#data-manager-search-form").serialize();
		card.formData = card.formData.replace(/%3A/g,":");
		card.formData = card.formData.replace(/%2B/g,"+");
		
		if(card.validateForm(card.formData) == -1){
			card.formData = "";
			return;
		}
		
		card.sessionStorageId = "search-"+card.id;
		card.operator = "none";
		card.tmp = false;
		
		sessionStorage.setItem(card.sessionStorageId,card.formData);
		
		createNewPanel(card.id,card.operator);
		
		//Finally we launch the animation
		card.goAway(function(){
			$("#search-error").fadeOut();
			$("#panel").fadeIn(function(){
				$("#data-manager-search-form").trigger("reset");
				card.comeBack();
			});
		});
		
		//finally we launch the animation
		var json = collectSessionStorageData();
		
		console.log("searching for results");
		
		searchSubmit(json);
	});
	
	$("#and-search-button").click(function(form){
		form.preventDefault();
		
		var card = cardArray[cardArray.length-1];

		console.log("card.id="+card.id);
		
		card.formData = $("#data-manager-search-form").serialize();
		card.formData = card.formData.replace(/%3A/g,":");
		card.formData = card.formData.replace(/%2B/g,"+");
		
		if(card.validateForm(card.formData) == -1){
			card.formData = "";
			return;
		}
		
		card.sessionStorageId = "search-"+card.id;
		card.operator = "AND";
		card.tmp = false;
		
		sessionStorage.setItem(card.sessionStorageId,card.formData);
		
		createNewPanel(card.id,card.operator);
		
		//Finally we launch the animation
		card.goAway(function(){
			$("#search-error").fadeOut();
			$("#panel").fadeIn(function(){
				$("#data-manager-search-form").trigger("reset");
				card.comeBack();
			});
		});
		
		$("#or-search-button").fadeOut();
		$("label[for=or-search-button]").fadeOut();
		$("#or-search-button").attr("disabled","disabled");
		
		createNewCard();
		//console.log(sessionStorage.getItem("search-1"));
		//console.log(sessionStorage.getItem("and-1"));
	});
	
	$("#or-search-button").click(function(form){
		form.preventDefault();
		
		var card = cardArray[cardArray.length-1];

		console.log("card.id="+card.id);
		
		card.formData = $("#data-manager-search-form").serialize();
		card.formData = card.formData.replace(/%3A/g,":");
		card.formData = card.formData.replace(/%2B/g,"+");
		
		if(card.validateForm(card.formData) == -1){
			card.formData = "";
			return;
		}
		
		card.sessionStorageId = "search-"+card.id;
		card.operator = "OR";
		card.tmp = false;
		
		sessionStorage.setItem(card.sessionStorageId,card.formData);
		
		createNewPanel(card.id,card.operator);
		
		//Finally we launch the animation
		card.goAway(function(){
			$("#search-error").fadeOut();
			$("#panel").fadeIn(function(){
				$("#data-manager-search-form").trigger("reset");
				card.comeBack();
			});
		});
		
		$("#and-search-button").fadeOut();
		$("label[for=and-search-button]").fadeOut();
		$("#and-search-button").attr("disabled","disabled");
		
		createNewCard();
		//console.log(sessionStorage.getItem("search-1"));
		//console.log(sessionStorage.getItem("and-1"));
	});
	
	$("#search-reduce").click(function(){
		
		var card = cardArray[cardArray.length-1];

		console.log("card.id="+card.id);
		
		card.formData = $("#data-manager-search-form").serialize();
		card.formData = card.formData.replace(/%3A/g,":");
		card.formData = card.formData.replace(/%2B/g,"+");
		card.sessionStorageId = "search-"+card.id;
		card.tmp = false;
		
		sessionStorage.setItem(card.sessionStorageId,card.formData);
		
		createNewPanel(card.id,card.operator);
		
		goAwaySearchCard(function(){
			$("#search-table").fadeIn();
			$("#panel").fadeIn();
			$("#new-search").fadeIn();
		});
		
	});	
	
	$("#add-params").click(function(){
		console.log("clicked");
				
		callPlusButtonsAnimation($("#add-dpo"),"add-dpo");
		setTimeout(function() {callPlusButtonsAnimation($("#add-start-time"),"add-start-time")},100);
		setTimeout(function() {callPlusButtonsAnimation($("#add-end-time"),"add-end-time")},200);
		setTimeout(function() {callPlusButtonsAnimation($("#add-organization"),"add-organization")},300);
		setTimeout(function() {callPlusButtonsAnimation($("#add-security"),"add-security")},400);
		
	});
	
});

$(function(){
	$("#new-search-button").click(function(){
		$("#panel").fadeOut(function(){
			$(this).empty();
		});
		$("#search-table").fadeOut(function(){
			$(this).empty();
		});
		$(this).fadeOut(function(){
			comeBackSearchCard();
		})
		
		sessionStorage.clear();
		
		nsearch = 0;
		nand = 0;
		nor = 0;
		npanel = 0;
		tmp_search = false;
	});
});

function createFirstCard(){
	
	if(nsearch>0)
		return; 
	
	var card = Object.create(protoSearchCard);
	card.id = nsearch++;
	cardArray.push(card);
}

function createNewCard(){
	var card = Object.create(protoSearchCard);
	card.id = nsearch++;
	cardArray.push(card);
}

function createNewPanel(cardId, operator){
	
	var card = cardArray[cardId];
	
	if (card.hasSearchPanel == true && card.hasOperatorPanel == true)
		return;
	
	if(card.hasSearchPanel == false){
		
		var panel = Object.create(protopanel);
		panel.id = npanel++;
		panel.searchId = card.id;
		panel.operator = "none";
		
		console.log("panel.id="+panel.id);
		console.log("panel.searchId="+panel.searchId);
		
		//panelArray.push(panel);
		
		var main_panel = $("#panel");
		var top = $(window).height()*0.3;
		var new_top = top + npanel*50;
		var text = "Search "+card.id;
		
		var htmlpanel = $("<div></div>");
		htmlpanel.attr({"class":"search-lateral-panel",
					"id": "panel-"+panel.id})
		         .css({"top": new_top+"px"})
		         .text(text)
		         .appendTo(main_panel)
		         .click(function(){
		        	 
		        	 //on click we must retrieve the card related to the panel
		        	 $(this).css("background-color","#c9e08e");
			    	 $("#panel .search-lateral-panel:not(#"+htmlpanel.attr("id")+")").css("background-color","#d3f1dc");
			    	 
			    	 //if the actual card is temporary, however, we must save 
			    	 //the actual card form and create a new panel in order to retrieve it
			    	 if(cardArray[cardArray.length-1].tmp == true){
			    		 cardArray[cardArray.length-1].tmp = false;
			    		 
			    		 cardArray[cardArray.length-1].formData = $("#data-manager-search-form").serialize();
			    		 cardArray[cardArray.length-1].formData = cardArray[cardArray.length-1].formData.replace(/%3A/g,":");
			    		 cardArray[cardArray.length-1].formData = cardArray[cardArray.length-1].formData.replace(/%2B/g,"+");
			    		 cardArray[cardArray.length-1].sessionStorageId = "search-"+cardArray.length-1;
			    		 sessionStorage.setItem(cardArray[cardArray.length-1].sessionStorageId,cardArray[cardArray.length-1].formData);
			    		 
			    		 createNewPanel(cardArray.length-1,"none");
			    	 }
			    	 
			    	 //Now we can do some animation:
			    	 // - eventually remove any card or table present
			    	 // - restore the values owned by the clicked search card
			    	 // - let the clicked search card come back
			    	 
			    	 var i = 0;
			    	 for(i=0; i<cardArray.length; i++){
			    		 if(cardArray[i].visibility === "visible"){
			    			 cardArray[i].visibility = "gone";
			    			 break;
			    		 }
			    	 }
			    	 
			    	 $("#search-table").fadeOut();
			    	 $("#new-search").fadeOut();
			    	 
			    	 cardArray[i].goAway(function(){
			    		 cardArray[panel.searchId].restoreForm();
			    		 cardArray[panel.searchId].comeBack();
			    		 cardArray[panel.searchId].visibility = "visible";
			    	 })
		         });
	
		panelArray.push(panel);
		cardArray[cardId].hasSearchPanel = true;
		         
	}
	
	if(card.operator === "none")
		return;
	
	if(card.hasOperatorPanel == false && card.tmp == false){
		
		var panel = Object.create(protopanel);
		panel.id = npanel++;
		panel.searchId = card.id;
		panel.operator = operator;
		
		//panelArray.push(panel);
		
		var main_panel = $("#panel");
		var top = $(window).height()*0.3;
		var new_top = top + npanel*50;
		var text = operator;
		
		var htmlpanel = $("<div></div>");
		htmlpanel.attr({"class":"search-lateral-panel",
					"id": "panel-"+panel.id})
		         .css({"top": new_top+"px"})
		         .text(text)
		         .appendTo(main_panel);
		
		panelArray.push(panel);
		cardArray[cardId].hasOperatorPanel = true;
	}
}

function collectSessionStorageData(){
	/*JSON model
	 * 
	 * [
	 *		{
	 *	  		"searchLocation": ["local","remote"],
	 *	  		"searchDataType": [".cef",".txt",".log",""],
	 *	  		"searchOrganizationCS": ["eq","ne"],
	 *	  		"searchOrganization": "string",
 	 *	  		"searchDpoId": "string",
	 *	  		"searchStartTimeCS": ["eq","ne","lt","lte","gt","gte"],
	 *	  		"searchStartTime": "YYYY-MM-DDThh:mm:ss.0Z",
	 *	  		"searchEndTimeCS": ["eq","ne","lt","lte","gt","gte"],
	 *	  		"searchEndTime": "YYYY-MM-DDThh:mm:ss.0Z",
	 *	  		"searchSecurityCS": ["eq","ne"],
	 *	  		"searchSecurityLevel": ["INFORMATIONAL","WARNING","MINOR","MAJOR","CRITICAL"],
	 *			"searchCombiningRule": ["and","or",""]
	 *		},
	 *		{
	 *			...
	 *		},
	 *		...
	 *		{
	 *			...
	 *		}
	 * ] 
	 * */
	
	/* {
	 *     "combingRule" : "and/or",
	 *     "criteria" : [{
	 *         "attribute" : "field name",
	 *         "operator" : "the logic operator",
	 *         "value" : "the actual value"
	 *     },
	 *     {
	 *     ....
	 *     }]
	 *}
	 * 
	 */
	
	var combining_rule = cardArray[0].operator;
	
	if(combining_rule === "none"){
		combining_rule = "";
	}
	
	var json = '{\n' +
		'"combining_rule": "' + combining_rule+ '",\n' +
		'"criteria": [';
	
	var last = false;
	
	for(var i=0; i<cardArray.length; i++){
		
		if(i == cardArray.length-1)
			last = true;
		
		var object = "";
		var queryString = sessionStorage.getItem(cardArray[i].sessionStorageId);
		var params = queryString.split("&");
		
		var conditionSelector = [];
		var conditionValue = [];
		var inputName = [];
		var inputValue = [];
		
		for(var j=0; j<params.length; j++){
		
			var keyValue = params[j].split("=");
			var key = keyValue[0];
			var value = keyValue[1];
			
			if(key.substr(key.length-2) === "CS" && value !== ""){
				conditionSelector.push(key);
				conditionValue.push(value);
				continue;
			}else if(key === "searchDpoId" && value !== ""){
				conditionSelector.push("searchDpoIdCS");
				conditionValue.push("eq");
				inputName.push(key);
				inputValue.push(value);
				continue;
			}else if(value !== ""){
				inputName.push(key);
				inputValue.push(value);
			}
		}
		
		for(var j=0; j<conditionSelector.length; j++){
			
			if(conditionSelector[j] === "searchOrganizationCS"){
				for(var k=0; k<inputName.length; k++){
					if(inputName[k] === "searchOrganization" ){
						object += objectBuilder(object,"organization",conditionValue[j],inputValue[k],last);
					}
				}
			}
			
			if(conditionSelector[j] === "searchStartTimeCS"){
				for(var k=0; k<inputName.length; k++){
					if(inputName[k] === "searchStartTime" ){
						object += objectBuilder(object,"start_time",conditionValue[j],inputValue[k],last);
					}
				}
			}
			
			if(conditionSelector[j] === "searchEndTimeCS"){
				for(var k=0; k<inputName.length; k++){
					if(inputName[k] === "searchEndTime" ){
						object += objectBuilder(object,"end_time",conditionValue[j],inputValue[k],last);
					}
				}
			}
			
			if(conditionSelector[j] === "searchSecurityCS"){
				for(var k=0; k<inputName.length; k++){
					if(inputName[k] === "searchSecurityLevel" ){
						object += objectBuilder(object,"severity_level",conditionValue[j],inputValue[k],last);
					}
				}
			}
			
			if(conditionSelector[j] === "searchDpoIdCS"){
				for(var k=0; k<inputName.length; k++){
					if(inputName[k] === "searchDpoId" ){
						object += objectBuilder(object,"id",conditionValue[j],inputValue[k],last);
					}
				}
			}
			
		}
		
		json += object;
	}
	
	json += ']}\n';
	console.log("json="+json);

	return json;
}

function objectBuilder(object,name,cs,val,last){
	
	object += '{\n' +
	    '"attribute": "'+name+'",\n' +
	    '"operator": "'+cs+'",\n' +
	    '"value": "'+val+'"\n';
	
	if(last == true){
		object += '}\n';
	}
	else{
		object += '},\n';
	}
	
	return object;
}

/************ UPLOAD DRAG AND DROP **************/

function readURL(input) {
    if (input.files && input.files[0]) {

    	var reader = new FileReader();

    	reader.onload = function(e) {
    		//$('.image-upload-wrap').hide();

    		$("#file-name").text(input.files[0].name);
		    //$('.file-upload-image').attr('src', e.target.result);
		    //$('.file-upload-content').show();
		
		    //$('.image-title').html(input.files[0].name);
    	};

    	reader.readAsDataURL(input.files[0]);

    } else {
    	removeUpload();
    }
}

function removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
}

$('.image-upload-wrap').bind('dragover', function () {
    $('.image-upload-wrap').addClass('image-dropping');
});

$('.image-upload-wrap').bind('dragleave', function () {
    $('.image-upload-wrap').removeClass('image-dropping');
});
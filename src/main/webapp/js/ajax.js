/*
 * Page: upload.jsp
 * Inputs involved: #organization-select 
 * Output: list of DSA-ID inside the #dsa-id-select <select> input
 * Use: this ajax sends the selected organization value to the getDsaId(Organization org) RESTapi
 * that returns a list of DSA-ID placed inside the #dsa-id-select <select> input
 */

$("#upload-organization-select").change(function() {
  // Check input( $( this ).val() ) for validity here
	var value = this.value;
	var data = "{\"organization\":\""+ value +"\"}";
	console.log("data="+data);
	showLoader();
	
	$.ajax({
		type: "POST",
		url: "/v1/getDsaId",
		data: data,
		contentType: "application/json",  
		dataType: "json",
		success: function(result){
			
			hideLoader();
			if(decodeError(result) == -1){
				$('#form-submit').hide();
				return;
			}
			else{			
				$("#upload-error").fadeOut();
				$('#form-submit').show();
			}
			
			var select = $("#upload-dsa-id-select");
			select.empty();
			
			for(var i=0; i<result.length; i++){
				var option = $("<option></option>").appendTo(select);
					option.text(result[i]);
					option.val(result[i]);
			}
			$("#select-div").fadeIn();
		},
		error: function(){
			hideLoader();
			decodeError("exception");
		}
	})
});


function searchSubmit(json){
	
	showLoader();
	
	$.ajax({
		url: "/v1/searchDpoId",
		type: "POST",
		contentType: "application/json",
		dataType: "json",
		data: json,
		success: function(result){
			console.log("succeess");
			hideLoader();
		},
		error: function(){
			console.log("failure");
			hideLoader();
			decodeError("exception");
		}
	})
}

function uploadSingleFile(file){
	
	showLoader();
	
	$.ajax({
		url: "/v1/uploadSingleFile",
		type: "POST",
		enctype: 'multipart/form-data',
		contentType: false,
		processData: false,
		data: file,
		cache: false,
		success: function(result){
			console.log("result="+result);
			hideLoader();
		},
		error: function(error){
			console.log("error="+error);
			hideLoader();
		}
	})
}

function uploadSingleFileMetadata(metadata){
	
	showLoader();
	
	$.ajax({
		url: "/v1/uploadSingleFileMetadata",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: metadata,
		success: function(result){
			console.log("result="+result);
			hideLoader();
		},
		error: function(error){
			console.log("error="+error);
			hideLoader();
		}
	})
}

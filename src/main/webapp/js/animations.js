function searchSubmitAnimation() {
	console.log("clicked");
	
	goAwaySearchCard(function(){		
		$("#panel").fadeIn();
		$("#search-table").fadeIn();
	});
		
	
}

function goAwaySearchCard(callback){
		
		var card = $("#search-card");
		var width = card.width();
		
		card.css({
			"position":"absolute",
			"width": width
		}).animate({
				left: "100%",
			},500
		).fadeOut(function(){
			
			if(callback)
				callback();
		});
		
}

function comeBackSearchCard(callback){
	
	$("#search-card").css({
		"position": "relative",
		"left": "100%"
	})
	.show().animate({
			left: "0%",
	},500,function(){
		
		if(callback)
			callback();
	});
	
}

function searchAndOrAnimation(search_card_count){
	
	var card = $("#search-card");
	var width = card.width();
	
	card.css({
		"position":"absolute",
		"width": width
	}).animate({
			left: "100%",
		},500
	).fadeOut(function(){
		$("#panel"+search_card_count).fadeIn();
		$("form").trigger("reset").fadeIn();
	});
	
}	

function callPlusButtonsAnimation(object,id){
	
	object.fadeIn(300,function(){
		plusButtonsAnimation(id,"30deg",function(){
			plusButtonsAnimation(id,"-30deg",function(){
				plusButtonsAnimation(id,"0deg");					
			});
		});
	});
}

function plusButtonsAnimation(id,angle,callback){
	
	var button = $("#"+id);
	
	button.css({
		"-webkit-transform": "rotate("+angle+")", 
		"-ms-transform": "rotate("+angle+")", 
		"transform": "rotate("+angle+")" 
	});
	
	setTimeout(function(){
		if(callback)
			callback();
	},300);
	
}

function showLoader(){
    $('#loader').fadeIn();
}

function hideLoader(){
    $('#loader').fadeOut();
}

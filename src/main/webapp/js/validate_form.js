/******************* SEARCH FORM ************************/
function validateSearchForm(form_data){
	
	//We validate organization fields
	if(validateOrganization() == -1)
		return -1;
	
	//We validate date fields
	if(validateDate() == -1)
		return -1;
	
	//We validate security selects
	if(validateSecurity() == -1)
		return -1;
	
	//We have to ensure that at most one of condition selector is provided
	if(validateConditionSelectors(form_data) == -1){
		return -1;
	}
	
	return 0;
}

function validateOrganization(){
	
	if($("#search-organization-select").val() !== "" && $("#search-organization").val().length == 0){
		console.log("2-required error in organization");
		decodeError("2-required","","search");
		return -1;
	}
	
	if($("#search-organization-select").val() === "" && $("#search-organization").val().length > 0){
		console.log("2-required error in organization");
		decodeError("2-required","","search");
		return -1;
	}
	
	return 0;
}

function validateDate(){
	
	if(validateDateRegex("search","start") == -1){
		console.log("wrong start date format");
		return -1;
	}

	if(validateDateRegex("search","end") == -1){
		console.log("wrong end date format");
		return -1;
	}
	
	if(validateDateInput("start") == -1){
		console.log("2-required error in start date");
		return -1;
	}
	
	if(validateDateInput("end") == -1){
		console.log("2-required error in end date");
		return -1;
	}
	
	return 0;
}

function validateDateInput(start_or_end){
	
	var time_select = $("#search-"+start_or_end+"-time-select").val();
	var time_input = $("#search-"+start_or_end+"-time").val();
	
	if(time_select !== "" && time_input.length == 0){
		decodeError("2-required","","search");
		return -1;
	}
	
	if(time_select === "" && time_input.length > 0){
		decodeError("2-required","","search");
		return -1;
	}
	
	return 0;
}

function validateSecurity(){
	
	if($("#search-security-select").val() !== "" && $("#search-level-select").val() === ""){
		console.log("2-required error security");
		decodeError("2-required","","search");
		return -1;
	}
	
	if($("#search-security-select").val() === "" && $("#search-level-select").val() !== ""){
		console.log("2-required error security");
		decodeError("2-required","","search");
		return -1;
	}
	
	return 0;
	
}

function validateConditionSelectors(form_data){
	
	var array = form_data.split("&");
	var count = 0;
	var max_count = 2;
	
	for(var i=0; i<array.length; i++){
		
		var field_value = array[i].split("=");
		var field = field_value[0];
		var value = field_value[1];
		
		//console.log(field+"="+value);
		//required field. We can skip it
		if(field === "searchLocation" || field === "searchDataType")
			continue;
		
		if(field === "searchDpoId" && value.length > 0)
			max_count = 1;
			
		if(value.length > 0)
			count++;
	}
	
	//console.log("count="+count);
	//console.log("max_count="+max_count);
	
	if(count == max_count && count > 0)
		return 0;
	else{
		decodeError("no-condition-selector","","search");
		return -1;
	}
}

/************************************************************/


/******************** COMMON FORM ***************************/

function validateDateRegex(card_type,start_or_end){
	
	var date_regex = new RegExp(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{1,}(Z|\+\d{0,})/gm);
	var time = $("#"+card_type+"-"+start_or_end+"-time").val(); 
	
	if(time.length > 0){
		//console.log("time.length="+time.length)
		if(time.match(date_regex) == null){
			console.log("time.match(date_regex)=null");
			decodeError("invalid",start_or_end+" time",card_type);
			return -1;
		}
	}
	
	return 0;
}

function restoreFormValues(values){
	var form_array = values.split("&");
	
	for(var i=0; i<form_array.length; i++){
		var field_value = form_array[i].split("=");
		var field = field_value[0];
		var value = field_value[1];
		
		if(field === "searchLocation"){
			if(value === "remote"){
				$("#data-manager-search-form input[name="+field+"]")[0].checked = true;
				$("#data-manager-search-form input[name="+field+"]")[1].checked = false;
			}
			else{
				$("#data-manager-search-form input[name="+field+"]")[0].checked = false;
				$("#data-manager-search-form input[name="+field+"]")[1].checked = true;
			}
			continue;
		}
		
		if(field === "searchDataType" || field === "searchOrganizationCS" || field === "searchStartTimeCS" ||
		   field === "searchEndTimeCS" || field === "searchSecurityCS" || field === "searchSecurityLevel"){
			$("#data-manager-search-form select[name="+field+"] option").each(function(){
				if($(this).val() === value){
					$(this).attr("selected","selected");
				} 
			});
			continue;
		}
		
		if(field === "searchOrganization" || field === "searchDpoId" || field === "searchStartTime" || 
		   field === "searchEndTime"){
			$("#data-manager-search-form input[name="+field+"]").val(value);
		}
		
	}
}

/**************************************************************/
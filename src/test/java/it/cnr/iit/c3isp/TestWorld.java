//package it.cnr.iit.c3isp;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.util.ArrayList;
//
//import iit.cnr.it.classificationengine.basic.DataSet;
//import iit.cnr.it.classificationengine.basic.Element;
//import iit.cnr.it.classificationengine.basic.TYPE;
//import it.cnr.iit.c3isp.mailiai.parser.GlobalVars;
//import it.cnr.iit.c3isp.mailiai.parser.Mail;
//import it.cnr.iit.c3isp.mailiai.parser.Parser;
//import it.cnr.iit.c3isp.mailiai.parser.Utilities;
//import it.cnr.iit.iai.implementation.MailAnalyzer;
//
//public class TestWorld {
//
//  public static void main(String[] args)
//      throws FileNotFoundException, Exception {
//    File file = new File(args[0]);
//    System.out.println(args[0]);
//    ArrayList<Mail> mails = new ArrayList<>();
//    ArrayList<String> names = new ArrayList<>();
//    DataSet<Integer> dataset = new DataSet<Integer>(Integer.class) {
//    };
//    dataset.setQualification(TYPE.CLUSTERING);
//    int i = 0;
//    if (file.isDirectory()) {
//      for (String string : file.list()) {
//        names.add(string);
//        File mailString = new File(file.getAbsolutePath() + "/" + string);
//        if (!mailString.isDirectory()) {
//          Mail mail = new Mail(i);
//          i += 1;
//          Parser parser = new Parser(
//              new FileInputStream(mailString.getAbsolutePath()), mail);
//          mails.add(mail);
//          parser.parse();
//
//          mail.setMessageType(GlobalVars.HTML_BASED);
//          String text = mail.getMailText();
//          if (mail.getMessageType() == GlobalVars.HTML_BASED) {
//            text = Utilities.html2text(text); //
//            // System.out.println("new text: " + text);
//          }
//          Element<Integer> element = Mail.getMailFeaturesWithID(mail);
//          if (dataset.getNumberOfFeatures() > 1
//              && element.size() != dataset.getNumberOfFeatures()) {
//            System.out.println("NOT ADDED");
//
//          } else {
//            dataset.add(element);
//          }
//        }
//      }
//      // MailAnalyzer classifier = MailAnalyzer.instantiateMailAnalyzer();
//      /*
//       * String[] result = classifier.classify(dataset).split("\n");
//       * ArrayList<Arc> arcs = new ArrayList<Arc>(); MailJSON mailJson = new
//       * MailJSON(); dataset.store("/home/antonio/datasetSpam.txt"); for (i = 0;
//       * i < result.length; i++) { MailClassified mailClassified = new
//       * MailClassified(mails.get(i)); mailClassified.setSpamClass(result[i]);
//       * System.out.println(names.get(i) + "\t" + result[i]); Arc arc =
//       * Arc.arcFromMailClassified(mailClassified); if (arc != null) {
//       * mailJson.addArc(arc); } } System.out.println(new
//       * Gson().toJson(mailJson)); }
//       */
//    }
//
//  }
//}

//
//package it.cnr.iit.c3isp;
//
//import java.io.IOException;
//import java.net.URI;
//import java.util.LinkedList;
//import java.util.concurrent.ExecutionException;
//
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.web.client.RestTemplateBuilder;
//import org.springframework.context.annotation.Bean;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.util.UriComponentsBuilder;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import eu.c3isp.iai.subapi.types.DpoResponses;
//import eu.c3isp.iai.subapi.types.Ticket;
//import eu.c3isp.isi.api.restapi.types.IAIParam;
//import eu.c3isp.isi.api.restapi.types.SearchParams;
//import eu.c3isp.isi.api.restapi.types.xacml.RequestAttributes;
//import eu.c3isp.isi.api.restapi.types.xacml.RequestContainer;
//import eu.c3isp.isi.api.restapi.types.xacml.RequestElement;
//import it.cnr.iit.utilty.Utility;
//
//@Controller
//public class DetectDGA {
//
//	@Autowired
//	private Utility utility;
//
//	@Autowired
//	private RestTemplate restTemplate;
//
//	public DetectDGA() {
//		restTemplate = restTemplate(new RestTemplateBuilder());
//	}
//
//	@Value("${security.user.name}")
//	private String restUser = "user";
//	@Value("${security.user.password}")
//	private String restPassword = "password";
//
//	@Bean
//	public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
//		RestTemplate restTemplate = restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
//		return restTemplate;
//	}
//
//	@Test
//	public void detectDGA() throws InterruptedException, ExecutionException, IOException {
//		String searchString = "{\"combining_rule\": \"or\",\"criteria\":"
//				+ " [{\"attribute\":\"id\",\"operator\": \"eq\",\"value\":"
//				+ " \"1539848581807-a0d3eb68-247e-411a-9bfe-7b86687d6676\"}]}, \"DataType\" : \"string\" } ] } }";
//		IAIParam iaiParam = new IAIParam();
//		iaiParam.setSearchCriteria(utility.convertFromJsonString(searchString, SearchParams.class));
//		HttpEntity<IAIParam> entity = new HttpEntity<>(iaiParam);
//		ResponseEntity<Ticket> response = restTemplate.exchange("https://iaic3isp.iit.cnr.it/iai-api/v1/detectDGA",
//				HttpMethod.POST, entity, Ticket.class);
//		System.out.println("Response: " + response.getBody().getValue());
//
//		String result = response.getBody().getValue();
//		System.out.println(result);
//		DpoResponses dpoResponses = restTemplate.getForObject(
//				"https://iaic3isp.iit.cnr.it/iai-api/v1/getResponse/{ticket}/", DpoResponses.class, result);
//		System.out.println(dpoResponses.getResults().get(0).getAdditionalProperties().get("dposId"));
//
//		RequestContainer container = new RequestContainer();
//		RequestElement el = new RequestElement();
//		RequestAttributes attr = new RequestAttributes();
//		attr.setAttributeId("ns:c3isp:read-dpo");
//
//		attr.setValue("read");
//		LinkedList<RequestAttributes> list = new LinkedList<RequestAttributes>();
//		list.add(attr);
//		el.setAttributes(list);
//		container.setRequest(el);
//		ObjectMapper mapper = new ObjectMapper();
//		System.err.println(mapper.writeValueAsString(container));
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//
//		headers.add("X-c3isp-input_metadata", mapper.writeValueAsString(container));
//
//		HttpEntity<RequestContainer> request = new HttpEntity<>(container, headers);
//		System.err.println(headers);
//
//		URI searchUri = UriComponentsBuilder.fromUriString("https://isic3isp.iit.cnr.it/isi-api/v1/dpo/")
//				.path("1540196729044-9e742f8d-64ba-4440-8ea2-5005df807f3c" + "/").build().toUri();
//
//		System.out.println(searchUri.toString());
//
//		result = restTemplate.exchange(searchUri, HttpMethod.GET, request, String.class).getBody();
//
//		System.out.println(result);
//
//		System.out.println("THE END");
//	}
//
//	public static void main(String args[]) {
//		try {
//			new DetectDGA().detectDGA();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//}
